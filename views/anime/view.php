<?php
$usersModel = new models\Users();
$animeModel = new models\Anime();
?>

<div>
    <div style="display: flex">
        <img src="/files/anime/<?= $model['photo'] ?>_s.jpg" alt="Anime image">
        <table style="margin-left: 15px" class="table">
            <tr>
                <th scope="row">Тип аніме</th>
                <td><?= $model['type'] ?></td>
            </tr>
            <tr>
                <th scope="row">Кількість епізодів</th>
                <td><?= $model['episodes_count'] ?></td>
            </tr>
            <tr>
                <th scope="row">Жанри</th>
                <td>
                    <?php
                    $genresArray = explode(', ', $model['genres']);
                    $genreNames = [];
                    foreach ($genresArray as $genreId) {
                        $genreIdInt = (int)$genreId;
                        $genreNames [] = $animeModel->GetGenreById($genreIdInt)[0]['name'];
                    }
                    $genreNamesStr = implode(', ', $genreNames);
                    echo "$genreNamesStr";
                    ?>
                </td>
            </tr>
            <tr>
                <th scope="row">Статус аніме</th>
                <td><?= $model['status'] ?></td>
            </tr>
            <tr>
                <th scope="row">Рейтинг MAL</th>
                <td><?= $model['rating'] ?></td>
            </tr>
            <tr>
                <th scope="row">Студія</th>
                <td><?= $model['studio_name'] ?></td>
            </tr>
            <tr>
                <th scope="row">Рік випуску</th>
                <td><?= $model['release_date'] ?></td>
            </tr>
        </table>
    </div>
    <div style="margin-top: 15px">
        <h5>Опис аніме</h5>
        <p>
            <?= $model['description'] ?>
        </p>
    </div>
    <h5>Відгуки про аніме</h5>
    <?php if ($model['lastReview'] != null) : ?>
        <div>
            <h6>Останній залишений відгук</h6>
            <div class="card" style="margin-bottom: 7px">
                <div class="card-header">
                    Відгук від користувача <?= $usersModel->GetUserById($model['lastReview']['user_id'])['nickname'] ?>
                </div>
                <div class="card-body">
                    <p class="card-text"><?= $model['lastReview']['short_text'] ?></p>
                    <a href="/reviews/view?id=<?=$model['lastReview']['id'] ?>" class="btn btn-primary mb-3">Читати далі</a>
                    <p class="card-text">
                        <small class="text-muted">
                            Час додавання: <?= $model['lastReview']['datetime'] ?>
                        </small>
                    </p>
                </div>
            </div>
        </div>
    <?php else : ?>
        <p>У даного аніме ще немає відгуків </p>
    <?php endif; ?>
    <a href="/reviews?type=anime&id=<?=$model['id']?>" class="btn btn-primary">Перейти на сторінку відгуків</a>
</div>
