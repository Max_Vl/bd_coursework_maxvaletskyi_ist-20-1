<?php
$modelUsers = new models\Users();
$user = $modelUsers->GetCurrentUser();
$animeModel = new models\Anime();
?>
<?php if ($user['role'] === "admin" || $user['role'] === "editor") : ?>
    <a href="/anime/add" class="btn btn-info mb-3">Додати аніме</a>
<?php endif; ?>
<div>
    <div class="aggregation-component">
        <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                    data-bs-toggle="dropdown" aria-expanded="false">
                Сортування..
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                <li><a class="dropdown-item"
                       href="/anime?page=<?= $model['currentPage'] ?>&sort=DESC&genre=<?= $model['currentGenre'] ?>">Спочатку
                        новіші</a></li>
                <li><a class="dropdown-item"
                       href="/anime?page=<?= $model['currentPage'] ?>&sort=ASC&genre=<?= $model['currentGenre'] ?>">Спочатку
                        старіші</a></li>
                <li><a class="dropdown-item"
                       href="/anime?page=<?= $model['currentPage'] ?>&sort=RatingASC&genre=<?= $model['currentGenre'] ?>">По
                        зростанню рейтингу</a></li>
                <li><a class="dropdown-item"
                       href="/anime?page=<?= $model['currentPage'] ?>&sort=RatingDESC&genre=<?= $model['currentGenre'] ?>">По
                        спаданню рейтингу</a></li>
            </ul>
        </div>
        <div>
            <?php
            switch ($model['currentSort']) {
                case 'DESC':
                    echo '<button type="button" class="btn btn-outline-primary" disabled>Спочатку новіші</button>';
                    break;
                case 'ASC':
                    echo '<button type="button" class="btn btn-outline-primary" disabled>Спочатку старіші</button>';
                    break;
                case 'RatingASC':
                    echo '<button type="button" class="btn btn-outline-primary" disabled>По зростанню рейтингу</button>';
                    break;
                case 'RatingDESC':
                    echo '<button type="button" class="btn btn-outline-primary" disabled>По спаданню рейтингу</button>';
                    break;
            }
            ?>
        </div>
    </div>
    <div class="aggregation-component">
        <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                    data-bs-toggle="dropdown" aria-expanded="false">
                Жанр..
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                <li><a class="dropdown-item"
                       href="/anime?page=<?= $model['currentPage'] ?>&sort=<?= $model['currentSort'] ?>&genre=all">Всі
                        жанри</a></li>
                <?php foreach ($model['genres'] as $genre) : ?>
                    <li><a class="dropdown-item"
                           href="/anime?page=<?= $model['currentPage'] ?>&sort=<?= $model['currentSort'] ?>&genre=<?= $genre['id'] ?>"><?= $genre['name'] ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div>
            <?php if ($model['currentGenre'] == 'all') : ?>
                <button type="button" class="btn btn-outline-primary" disabled>Всі жанри</button>
            <?php else : ?>
                <button type="button" class="btn btn-outline-primary"
                        disabled><?= $animeModel->GetGenreById($model['currentGenre'])[0]['name'] ?></button>
            <?php endif; ?>
        </div>
    </div>
</div>
<div style="display: flex; flex-wrap: wrap; justify-content: space-around;">
    <?php for ($i = $model['currentPage'] * $model['animePerPage']; $i < $model['currentPage'] * $model['animePerPage'] + $model['animePerPage']; $i++) : ?>
        <?php if (!empty($model['anime'][$i])) : ?>
            <div class="card" style="width: 18rem; margin: 5px;">
                <?php if (is_file('files/anime/' . $model['anime'][$i]['photo'] . '_s.jpg')) : ?>
                    <img class="card-img-top" src="/files/anime/<?= $model['anime'][$i]['photo'] ?>_s.jpg">
                <?php endif; ?>
                <div class="card-body">
                    <h5 class="card-title" style="text-align: center"><?= $model['anime'][$i]['title'] ?></h5>
                    <p class="card-text"><b>Жанри аніме:</b>
                        <?php
                        $genresArray = explode(', ', $model['anime'][$i]['genres']);
                        $genreNames = [];
                        foreach ($genresArray as $genreId) {
                            $genreIdInt = (int)$genreId;
                            $genreNames [] = $animeModel->GetGenreById($genreIdInt)[0]['name'];
                        }
                        $genreNamesStr = implode(', ', $genreNames);
                        echo "$genreNamesStr";
                        ?>
                    </p>
                    <p class="card-text"><b>Статус аніме:</b> <?= $model['anime'][$i]['status'] ?></p>
                    <p class="card-text"><b>Рейтинг аніме:</b> <?= $model['anime'][$i]['rating'] ?></p>
                    <div style="text-align: center">
                        <a href="/anime/view?id=<?= $model['anime'][$i]['id'] ?>" class="btn btn-primary mb-3">Детальніше</a>
                        <?php if ($user['role'] === "editor" || $user['role'] === "admin") : ?>
                            <a href="/anime/edit?id=<?= $model['anime'][$i]['id'] ?>"
                               class="btn btn-success mb-3">Редагувати</a>
                            <a href="/anime/delete?id=<?= $model['anime'][$i]['id'] ?>"
                               class="btn btn-danger mb-3">Видалити</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endfor; ?>
</div>
<div style="text-align: center">
    <div class="pagination">
        <a href="/anime?page=<?= $model['previousPage'] ?>&sort=<?= $model['currentSort'] ?>&genre=<?= $model['currentGenre'] ?>">&laquo;</a>
        <?php for ($i = 0; $i < $model['pagesCount']; $i++) : ?>
            <a href="/anime?page=<?= $i ?>&sort=<?= $model['currentSort'] ?>&genre=<?= $model['currentGenre'] ?>"><?= $i + 1 ?></a>
        <?php endfor; ?>
        <a href="/anime?page=<?= $model['nextPage'] ?>&sort=<?= $model['currentSort'] ?>&genre=<?= $model['currentGenre'] ?>">&raquo;</a>
    </div>
</div>

