<?php
$genresArray = explode(', ', $model['genres']);
?>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="title">Назва</label>
        <input type="text" class="form-control" id="title" name="title" value="<?=$model['title']?>">
    </div>
    <div class="form-group">
        <label for="type">Тип</label>
        <input type="text" class="form-control" id="type" name="type" value="<?=$model['type']?>" >
    </div>
    <div class="form-group">
        <label for="episodes_count">Кількість епізодів</label>
        <input type="number" class="form-control" id="episodes_count" name="episodes_count" value="<?=$model['episodes_count']?>">
    </div>
    <div class="form-group">
        <label for="status">Статус</label>
        <input type="text" class="form-control" id="status" name="status" value="<?=$model['status']?>">
    </div>
    <div class="form-group">
        <label for="rating">Рейтинг</label>
        <input type="number" step="0.1" class="form-control" id="rating" name="rating" value="<?=$model['rating']?>">
    </div>
    <div class="form-group">
        <label for="description">Опис</label>
        <textarea name="description" type="text" class="form-control editor"
                  id="description">
            <?= $model['description'] ?>
        </textarea>
    </div>
    <div class="form-group">
        <label for="studio_name">Студія</label>
        <input type="text" class="form-control" id="studio_name" name="studio_name" value="<?=$model['studio_name']?>">
    </div>
    <div class="form-group">
        <label for="release_date">Дата виходу</label>
        <input type="date" class="form-control" id="release_date" name="release_date" value="<?=$model['release_date']?>">
    </div>
    <div class="form-group">
        <label for="genres">Жанри</label>
        <select class="form-control" id="genres" name="genres[]" multiple="multiple">
            <?php
            foreach($genres as $genre) {
                if (in_array($genre['id'], $genresArray))
                    echo "<option value='" . $genre['id'] . "' selected>" . $genre['name'] . "</option>";
                else
                    echo "<option value='" . $genre['id'] . "'>" . $genre['name'] . "</option>";
            }
            ?>
        </select>
    </div>
    <div class="mb-3">
        <label for="file" class="form-label">Фото: </label>
        <input type="file" name="file" accept="image/jpeg, image/png" id="file" class="form-control">
    </div>
    <div class="mb-3">
        <?php if(is_file('files/news/'.$model['photo'].'_b.jpg')) : ?>
            <img src="/files/news/<?=$model['photo']?>_b.jpg" alt="photo">
        <?php endif; ?>
    </div>
    <button type="submit" class="btn btn-primary">Зберегти</button>
</form>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function() {
        $('#genres').select2({
            tags: true
        });
    });
</script>