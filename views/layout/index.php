<?php
$UserModel = new \models\Users();
$user = $UserModel->GetCurrentUser();
?>
<!DOCTYPE html>
<html lang="ua">
<head>
    <title><?= $MainTitle ?></title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="/style.css" type="text/css">
    <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>
    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css"
    />
    <style>
        body {
            background-color: #f8f9fa;
            display: flex;
            flex-direction: column;
            min-height: 100vh;
        }
        .navbar {
            background-color: #d63384;
            color: #fff;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .navbar-brand, .nav-link {
            color: #fff;
        }
        .navbar-brand:hover, .nav-link:hover {
            color: #ffc107;
        }
        .container {
            flex: 1;
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .footer {
            background-color: #d63384;
            color: #fff;
            padding: 1rem 0;
            text-align: center;
        }
        .footer a {
            text-decoration: none;
            color: #fff;
        }
        .sticky-top {
            position: sticky;
            top: 0;
            z-index: 1020;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light sticky-top">
    <div class="container-fluid">
        <a class="navbar-brand" href="/">AniMangaDB</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link" href="/news?page=0&sort=DESC&author=all">Новини</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/anime?page=0&sort=DESC&genre=all">Аніме</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/manga?page=0&sort=DESC&genre=all">Манга</a>
                </li>
            </ul>
            <form class="d-flex">
                <?php if ($user == null) : ?>
                    <a class="btn btn-outline-light me-2" href="/users/registration">Реєстрація</a>
                    <a class="btn btn-light" href="/users/login">Увійти</a>
                <?php else : ?>
                    <a class="btn btn-outline-light me-2" style="text-decoration: none"
                       href="/users/profile?user_id=<?= $user['id'] ?>">
                        <?= $user['nickname'] ?>
                    </a>
                    <a class="btn btn-light" href="/users/logout">Вийти</a>
                <?php endif; ?>
            </form>
        </div>
    </div>
</nav>

<div class="container">
    <h1 class="mt-5"><?= $PageTitle ?></h1>
    <? if (!empty($MessageText)) : ?>
        <div class="alert alert-<?= $MessageClass ?>" role="alert">
            <?= $MessageText ?>
        </div>
    <? endif; ?>
    <?= $PageContent ?>
</div>

<footer class="footer">
    <div class="container">
        <p>&copy; 2024 AniMangaDB. Всі права захищені.</p>
        <a href="#">Контакти</a> | <a href="#">Умови використання</a> | <a href="#">Політика конфіденційності</a>
    </div>
</footer>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz"
        crossorigin="anonymous"></script>
<?php if ($UserModel->isUserAuthenticated() && $UserModel->GetCurrentUser()['role'] != 'user') : ?>
    <script src="https://cdn.ckeditor.com/ckeditor5/41.0.0/classic/ckeditor.js"></script>
    <script>
        let editors = document.querySelectorAll('.editor');
        for (let i in editors) {
            ClassicEditor
                .create(editors[i])
                .catch(error => {
                    console.error(error);
                });
        }
    </script>
<?php endif; ?>

<script>
    Fancybox.bind("[data-fancybox]", {
        // Your custom options
    });
</script>
</body>
</html>