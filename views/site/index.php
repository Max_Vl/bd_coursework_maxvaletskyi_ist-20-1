<style>
    .section {
        margin-bottom: 40px;
    }
    .section-header {
        margin-bottom: 20px;
    }
</style>
<div class="container">
    <div class="section text-center">
        <div class="section-header">
            <h1>Ласкаво просимо до AniMangaDB!</h1>
        </div>
        <p>AniMangaDB - це база даних аніме та манги, де ви можете знайти інформацію про свої улюблені аніме та мангу!</p>
    </div>

    <div class="section">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        Останні надходження аніме
                    </div>
                    <div class="card-body">
                        <div id="animeCarousel" class="carousel slide" data-bs-ride="carousel">
                            <div class="carousel-inner">
                                <?php foreach ($model['lastAnime'] as $index => $anime): ?>
                                    <div class="carousel-item <?= $index === 0 ? 'active' : '' ?>">
                                        <a href="/anime/view?id=<?= $anime['id'] ?>">
                                            <img src="files/anime/<?= $anime['photo'] ?>_m.jpg" class="d-block w-100"
                                                 alt="<?= $anime['title'] ?>">
                                        </a>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <button class="carousel-control-prev" type="button" data-bs-target="#animeCarousel"
                                    data-bs-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Previous</span>
                            </button>
                            <button class="carousel-control-next" type="button" data-bs-target="#animeCarousel"
                                    data-bs-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Next</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        Останні надходження манги
                    </div>
                    <div class="card-body">
                        <div id="mangaCarousel" class="carousel slide" data-bs-ride="carousel">
                            <div class="carousel-inner">
                                <?php foreach ($model['lastManga'] as $index => $manga): ?>
                                    <div class="carousel-item <?= $index === 0 ? 'active' : '' ?>">
                                        <a href="/manga/view?id=<?= $manga['id'] ?>">
                                            <img src="files/manga/<?= $manga['photo'] ?>_m.jpg" class="d-block w-100"
                                                 alt="<?= $manga['title'] ?>">
                                        </a>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <button class="carousel-control-prev" type="button" data-bs-target="#mangaCarousel"
                                    data-bs-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Previous</span>
                            </button>
                            <button class="carousel-control-next" type="button" data-bs-target="#mangaCarousel"
                                    data-bs-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Next</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Останні новини
                    </div>
                    <div class="card-body">
                        <?php foreach ($model['lastNews'] as $news): ?>
                            <div class="card mb-3">
                                <div class="card-body">
                                    <h5 class="card-title"><?= $news['title'] ?></h5>
                                    <p class="card-text"><?= $news['short_description'] ?></p>
                                    <a href="/news/view?id=<?=$news['id']?>" class="btn btn-primary">Читати далі</a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
