<p>
    Ви дійсно бажаєте видалити вибраний відгук?
</p>
<p>
    <a href="/reviews/delete?id=<?=$model['id'] ?>&confirm=yes" class="btn btn-danger">Видалити</a>
    <a href="<?=$_SERVER['HTTP_REFERER']?>?>" class="btn btn-primary">Відмінити</a>
</p>