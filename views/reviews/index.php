<?php
$modelUsers = new models\Users();
$user = $modelUsers->GetCurrentUser();

if($model['reviewType'] == 'anime')
    $reviewTargetText = "аніме";
else
    $reviewTargetText = "мангу";
?>
<h3>Відгуки на <?= $reviewTargetText ?> "<?= $model['reviewTarget']['title'] ?>"</h3>
<?php  if($user != null) : ?>
    <a href="/reviews/add?targetId=<?=$model['targetId']?>&type=<?=$model['reviewType']?>" class="btn btn-info mb-3">Додати відгук</a>
<?php endif; ?>
<?php foreach ($model['freshReviews'] as $review) : ?>
    <div class="card" style="margin-bottom: 7px">
        <div class="card-header">
            Відгук від користувача <?= $modelUsers->GetUserById($review['user_id'])['nickname'] ?>
        </div>
        <div class="card-body">
            <p class="card-text"><?= $review['short_text'] ?></p>
            <a href="/reviews/view?id=<?=$review['id'] ?>" class="btn btn-primary mb-3">Читати далі</a>
            <?php  if($user != null) : ?>
                <?php  if($review['user_id'] == $user['id'] || $user['access'] == '1') : ?>
                    <a href="/reviews/edit?id=<?=$review['id'] ?>" class="btn btn-success mb-3">Редагувати</a>
                    <a href="/reviews/delete?id=<?=$review['id'] ?>" class="btn btn-danger mb-3">Видалити</a>
                <?php endif; ?>
            <?php endif; ?>
            <p class="card-text">
                <small class="text-muted">
                    Час додавання: <?= $review['datetime'] ?>
                </small>
            </p>
        </div>
    </div>
<?php endforeach; ?>
