<form action="" method="post">
    <div class="mb-3">
        <label for="short_text" class="form-label">Короткий текст відгуку</label>
        <input type="text" name="short_text" value="<?= $model['short_text']?>" class="form-control" id="title" aria-describedby="emailHelp">
    </div>
    <div class="mb-3">
        <label for="text" class="form-label">Повний текст відгуку</label>
        <textarea name="text" class="form-control editor" id="text"><?= $model['text']?></textarea>
    </div>
    <div style="text-align: center;">
        <h5>Блоки для оцінювання</h5>
    </div>
    <div style="display: flex; flex-wrap: wrap; justify-content: space-around">
        <div class="card" style="width: 14rem;">
            <div class="card-body">
                <h5 class="card-title" style="text-align: center">Сюжет</h5>
                <select id="story" name="story" class="form-select" aria-label="Default select example">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>
            </div>
        </div>
        <div class="card" style="width: 14rem;">
            <div class="card-body">
                <h5 class="card-title" style="text-align: center">Драматургія</h5>
                <select id="drama" name="drama" class="form-select" aria-label="Default select example">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>
            </div>
        </div>
        <div class="card" style="width: 14rem;">
            <div class="card-body">
                <h5 class="card-title" style="text-align: center">Персонажі</h5>
                <select id="characters" name="characters" class="form-select" aria-label="Default select example">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>
            </div>
        </div>
        <div class="card" style="width: 14rem;">
            <div class="card-body">
                <h5 class="card-title" style="text-align: center">Атмосферність</h5>
                <select id="atmosphere" name="atmosphere" class="form-select" aria-label="Default select example">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary" style="margin-top: 7px">Зберегти</button>
</form>

<script>
    document.querySelector('#story').value = <?= $marks['story']?>;
    document.querySelector('#drama').value = <?= $marks['drama']?>;
    document.querySelector('#characters').value = <?= $marks['characters']?>;
    document.querySelector('#atmosphere').value = <?= $marks['atmosphere']?>;
</script>