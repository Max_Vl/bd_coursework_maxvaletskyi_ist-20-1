<a href="<?= $_SERVER['HTTP_REFERER'] ?>" class="btn btn-primary">
    Повернутися назад
</a>
<p>
    <?= $model['text'] ?>
</p>
<div style="display: flex; flex-wrap: wrap; justify-content: space-around">
    <div class="card mark-container
    <?php if ((int)$marks['story'] < 5) : ?>
     negativeMark
    <?php elseif ((int)$marks['story'] < 8) : ?>
     middleMark
    <?php else : ?>
     positiveMark
    <?php endif; ?>" style="width: 12rem;">
        <div class="card-body">
            <h5 class="card-title" style="text-align: center">Сюжет</h5>
            <p style="text-align: center"><?= $marks['story'] ?></p>
        </div>
    </div>
    <div class="card mark-container
    <?php if ((int)$marks['drama'] < 5) : ?>
     negativeMark
    <?php elseif ((int)$marks['drama'] < 8) : ?>
     middleMark
    <?php else : ?>
     positiveMark
    <?php endif; ?>" style="width: 12rem;">
        <div class="card-body">
            <h5 class="card-title" style="text-align: center">Драматургія</h5>
            <p style="text-align: center"><?= $marks['drama'] ?></p>
        </div>
    </div>
    <div class="card mark-container
    <?php if ((int)$marks['characters'] < 5) : ?>
     negativeMark
    <?php elseif ((int)$marks['characters'] < 8) : ?>
     middleMark
    <?php else : ?>
     positiveMark
    <?php endif; ?>" style="width: 12rem;">
        <div class="card-body">
            <h5 class="card-title" style="text-align: center">Персонажі</h5>
            <p style="text-align: center"><?= $marks['characters'] ?></p>
        </div>
    </div>
    <div class="card mark-container
    <?php if ((int)$marks['atmosphere'] < 5) : ?>
     negativeMark
    <?php elseif ((int)$marks['atmosphere'] < 8) : ?>
     middleMark
    <?php else : ?>
     positiveMark
    <?php endif; ?>" style="width: 12rem;">
        <div class="card-body">
            <h5 class="card-title" style="text-align: center">Атмосфера</h5>
            <p style="text-align: center"><?= $marks['atmosphere'] ?></p>
        </div>
    </div>
</div>
<?php
$generalMark = ((int)$marks['story'] + (int)$marks['drama'] + (int)$marks['characters'] +
        (int)$marks['comedy'] + (int)$marks['atmosphere']) / 4
?>
<div style="text-align: center; display: flex; justify-content: center; margin-top: 7px">
    <div class="card
        <?php if ($generalMark < 5) : ?>
     negativeMark
    <?php elseif ($generalMark < 7) : ?>
     middleMark
    <?php else : ?>
     positiveMark
    <?php endif; ?>" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title">Загальна оцінка</h5>
            <p class="card-text">
                <?= $generalMark ?>
            </p>
        </div>
    </div>
</div>
<style>
    p {
        font-size: 18px;
    }

    .mark-container {
        margin: 5px;
    }

    .positiveMark {
        background-color: #45ff45;
    }

    .middleMark {
        background-color: #ffbe45;
    }

    .negativeMark {
        background-color: #ff3434;
    }
</style>