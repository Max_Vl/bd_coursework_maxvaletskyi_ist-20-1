<?php
$modelUsers = new models\Users();
$user = $modelUsers->GetCurrentUser();
$mangaModel = new models\Manga();
?>
<?php if ($user['role'] === "admin" || $user['role'] === "editor") : ?>
    <a href="/manga/add" class="btn btn-info mb-3">Додати мангу</a>
<?php endif; ?>
<div>
    <div class="aggregation-component">
        <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                    data-bs-toggle="dropdown" aria-expanded="false">
                Сортування..
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                <li><a class="dropdown-item"
                       href="/manga?page=<?= $model['currentPage'] ?>&sort=DESC&genre=<?= $model['currentGenre'] ?>">Спочатку
                        новіші</a></li>
                <li><a class="dropdown-item"
                       href="/manga?page=<?= $model['currentPage'] ?>&sort=ASC&genre=<?= $model['currentGenre'] ?>">Спочатку
                        старіші</a></li>
            </ul>
        </div>
        <div>
            <?php
            switch ($model['currentSort']) {
                case 'DESC':
                    echo '<button type="button" class="btn btn-outline-primary" disabled>Спочатку новіші</button>';
                    break;
                case 'ASC':
                    echo '<button type="button" class="btn btn-outline-primary" disabled>Спочатку старіші</button>';
                    break;
            }
            ?>
        </div>
    </div>
    <div class="aggregation-component">
        <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                    data-bs-toggle="dropdown" aria-expanded="false">
                Жанр..
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                <li><a class="dropdown-item"
                       href="/manga?page=<?= $model['currentPage'] ?>&sort=<?= $model['currentSort'] ?>&genre=all">Всі
                        жанри</a></li>
                <?php foreach ($model['genres'] as $genre) : ?>
                    <li><a class="dropdown-item"
                           href="/manga?page=<?= $model['currentPage'] ?>&sort=<?= $model['currentSort'] ?>&genre=<?= $genre['id'] ?>"><?= $genre['name'] ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div>
            <?php if ($model['currentGenre'] == 'all') : ?>
                <button type="button" class="btn btn-outline-primary" disabled>Всі жанри</button>
            <?php else : ?>
                <button type="button" class="btn btn-outline-primary"
                        disabled><?= $mangaModel->GetGenreById($model['currentGenre'])[0]['name'] ?></button>
            <?php endif; ?>
        </div>
    </div>
</div>
<div style="display: flex; flex-wrap: wrap; justify-content: space-around;">
    <?php foreach ($model['manga'] as $singleManga) : ?>
        <div class="card" style="width: 18rem; margin: 5px;">
            <? if (is_file('files/manga/' . $singleManga['photo'] . '_s.jpg')) : ?>
                <img class="card-img-top" src="/files/manga/<?= $singleManga['photo'] ?>_s.jpg">
            <? endif; ?>
            <div class="card-body">
                <h5 class="card-title" style="text-align: center"><?= $singleManga['title'] ?></h5>
                <p class="card-text"><b>Жанри манги:</b>
                    <?php
                    $genresArray = explode(', ', $singleManga['genres']);
                    $genreNames = [];
                    foreach ($genresArray as $genreId) {
                        $genreIdInt = (int)$genreId;
                        $genreNames [] = $mangaModel->GetGenreById($genreIdInt)[0]['name'];
                    }
                    $genreNamesStr = implode(', ', $genreNames);
                    echo "$genreNamesStr";
                    ?>
                </p>
                <p class="card-text"><b>Статус манги:</b> <?= $singleManga['status'] ?></p>
                <div style="text-align: center">
                    <a href="/manga/view?id=<?= $singleManga['id'] ?>" class="btn btn-primary mb-3">Детальніше</a>
                    <?php if ($user['role'] === "editor" || $user['role'] === "admin") : ?>
                        <a href="/manga/edit?id=<?= $singleManga['id'] ?>" class="btn btn-success mb-3">Редагувати</a>
                        <a href="/manga/delete?id=<?= $singleManga['id'] ?>" class="btn btn-danger mb-3">Видалити</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<div style="text-align: center">
    <div class="pagination">
        <a href="/manga?page=<?= $model['previousPage'] ?>&sort=<?= $model['currentSort'] ?>&genre=<?= $model['currentGenre'] ?>">&laquo;</a>
        <?php for ($i = 0; $i < $model['pagesCount']; $i++) : ?>
            <a href="/manga?page=<?= $i ?>&sort=<?= $model['currentSort'] ?>&genre=<?= $model['currentGenre'] ?>"><?= $i + 1 ?></a>
        <?php endfor; ?>
        <a href="/manga?page=<?= $model['nextPage'] ?>&sort=<?= $model['currentSort'] ?>&genre=<?= $model['currentGenre'] ?>">&raquo;</a>
    </div>
</div>