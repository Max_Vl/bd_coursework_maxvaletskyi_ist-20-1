<form method="post" action="">
    <div class="mb-3">
        <label for="InputEmail" class="form-label">Логін (email): </label>
        <input name="login" type="email" value="<?=$model['login'] ?>" class="form-control" id="InputEmail" aria-describedby="emailHelp">
    </div>
    <div class="mb-3">
        <label for="InputNickname" class="form-label">Нікнейм: </label>
        <input name="nickname" type="text" value="<?=$model['nickname'] ?>" class="form-control" id="InputNickname">
    </div>
    <button type="submit" class="btn btn-primary">Зберегти</button>
</form>