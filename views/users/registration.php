<form method="post" action="">
    <div class="mb-3">
        <label for="InputEmail" class="form-label">Логін (email): </label>
        <input name="login" type="email" value="<?=$_POST['login'] ?>" class="form-control" id="InputEmail" aria-describedby="emailHelp">
        <div id="emailHelp" class="form-text">Конфіденційність вашої email-адреси у безпеці</div>
    </div>
    <div class="mb-3">
        <label for="InputPassword1" class="form-label">Пароль: </label>
        <input name="password" type="password" class="form-control" id="InputPassword1">
    </div>
    <div class="mb-3">
        <label for="InputPassword2" class="form-label">Пароль (ще раз): </label>
        <input name="password2" type="password" class="form-control" id="InputPassword2">
    </div>
    <div class="mb-3">
        <label for="InputNickname" class="form-label">Нікнейм: </label>
        <input name="nickname" type="text" value="<?=$_POST['nickname'] ?>" class="form-control" id="InputNickname">
    </div>
    <button type="submit" class="btn btn-primary">Реєстрація</button>
</form>