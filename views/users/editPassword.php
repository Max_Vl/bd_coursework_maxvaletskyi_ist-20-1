<form method="post" action="">
    <div class="mb-3">
        <label for="oldPassword" class="form-label">Старий пароль: </label>
        <input name="oldPassword" type="password" class="form-control" id="oldPassword">
    </div>
    <div class="mb-3">
        <label for="InputPassword1" class="form-label">Новий пароль: </label>
        <input name="password" type="password" class="form-control" id="InputPassword1">
    </div>
    <div class="mb-3">
        <label for="InputPassword2" class="form-label">Новий пароль (ще раз): </label>
        <input name="password2" type="password" class="form-control" id="InputPassword2">
    </div>
    <button type="submit" class="btn btn-primary">Реєстрація</button>
</form>