<?php
$UserModel = new \models\Users();
$user = $UserModel->GetCurrentUser();
$displayedRole = '';
switch($user['role']) {
    case 'editor':
        $displayedRole = 'Редактор';
        break;
    case 'admin':
        $displayedRole = 'Адміністратор';
        break;
    default:
        $displayedRole = 'Користувач';
        break;
}
?>
<div>
    <table class="table">
        <thead>
            <tr>
                <th></th>
                <td></td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Пошта: </th>
                <td><?= $user['login'] ?></td>
            </tr>
            <tr>
                <th>Нікнейм: </th>
                <td><?= $user['nickname'] ?></td>
            </tr>
            <tr>
                <th scope="row">Роль: </th>
                <td><?= $displayedRole ?></td>
            </tr>
        </tbody>
    </table>
</div>
<div>
    <a class="btn btn-primary me-2" href="/users/edit?user_id=<?=$user['id']?>">Редагувати</a>
    <a class="btn btn-primary" href="/users/editPassword?user_id=<?=$user['id']?>">Змінити пароль</a>
</div>
