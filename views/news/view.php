<div class="news">
    <div>
        <?php if (is_file('files/news/' . $model['photo'] . '_m.jpg')) : ?>
            <?php if (is_file('files/news/' . $model['photo'] . '_b.jpg')) : ?>
                <a href="/files/news/<?= $model['photo'] ?>_b.jpg" data-fancybox="gallery">
            <?php endif; ?>
            <img class="bd-placeholder-img rounded float-start" src="/files/news/<?= $model['photo'] ?>_m.jpg" alt="photo">
            <?php if (is_file('files/news/' . $model['photo'] . '_b.jpg')) : ?>
                </a>
            <?php endif; ?>
        <?php endif; ?>
    </div>
    <div>
        <?= $model['text'] ?>
    </div>
</div>
<div class="text-center mt-3">
    <a href="/news/addComment?news_id=<?=$model['id']?>&user_id=<?=$model['reader_id']?>" class="btn btn-primary">Додати коментар</a>
</div>
<div class="container mt-5">
    <?php if ($model['lastComments'] != null) :?>
        <?php foreach ($model['lastComments'] as $comment) : ?>
            <div class="card mb-3">
                <div class="card-body">
                    <h5 class="card-title"><?= $model['userModel']->GetUserById($comment['user_id'])['nickname'] ?></h5>
                    <p class="card-text"><?= $comment['text'] ?></p>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="card mb-3">
            <div class="card-body">
                <p class="card-text">Коментарі відсутні</p>
            </div>
        </div>
    <?php endif; ?>
</div>