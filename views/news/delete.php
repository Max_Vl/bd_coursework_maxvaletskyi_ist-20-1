<div>
    <p>
        Видалити новину <b>"<?= $model['title'] ?>"</b>?
    </p>
</div>
<div>
    <a href="/news/delete?id=<?= $model['id'] ?>&confirm=yes" class="btn btn-danger">Видалити</a>
    <a href="<?= $_SERVER['HTTP_REFERER'] ?>" class="btn btn-primary">Відміна</a>
</div>
