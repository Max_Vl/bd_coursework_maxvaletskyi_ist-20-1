<?php
$userModel = new \models\Users();
?>
<div>
    <?php if ($userModel->GetCurrentUser()['role'] == 'admin' || $userModel->GetCurrentUser()['role'] == 'editor') : ?>
        <a href="/news/add" class="btn btn-primary">Додати новину</a>
    <?php endif; ?>
</div>
<div>
    <div class="aggregation-component">
        <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                    data-bs-toggle="dropdown" aria-expanded="false">
                Сортування..
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                <li><a class="dropdown-item"
                       href="/news?page=<?= $model['currentPage'] ?>&sort=DESC&author=<?= $model['currentAuthor'] ?>">Спочатку
                        новіші</a></li>
                <li><a class="dropdown-item"
                       href="/news?page=<?= $model['currentPage'] ?>&sort=ASC&author=<?= $model['currentAuthor'] ?>">Спочатку
                        старіші</a></li>
            </ul>
        </div>
        <div>
            <?php if ($model['sort'] == 'DESC') : ?>
                <button type="button" class="btn btn-outline-primary" disabled>Спочатку новіші</button>
            <?php else : ?>
                <button type="button" class="btn btn-outline-primary" disabled>Спочатку старіші</button>
            <?php endif; ?>
        </div>
    </div>
    <div class="aggregation-component">
        <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                    data-bs-toggle="dropdown" aria-expanded="false">
                Автор..
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                <li><a class="dropdown-item"
                       href="/news?page=<?= $model['currentPage'] ?>&sort=<?= $model['sort'] ?>&author=all">Всі
                        автори</a></li>
                <?php foreach ($authors as $authorId) : ?>
                    <?php $authorData = $userModel->GetUserById($authorId) ?>
                    <li><a class="dropdown-item"
                           href="/news?page=<?= $model['currentPage'] ?>&sort=<?= $model['sort'] ?>&author=<?= $authorData['id'] ?>"><?= $authorData['nickname'] ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div>
            <?php if ($model['currentAuthor'] == 'all') : ?>
                <button type="button" class="btn btn-outline-primary" disabled>Всі автори</button>
            <?php else : ?>
                <button type="button" class="btn btn-outline-primary"
                        disabled><?= $userModel->GetUserById($model['currentAuthor'])['nickname'] ?></button>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php for ($i = $model['currentPage'] * $model['newsPerPage']; $i < $model['currentPage'] * $model['newsPerPage'] + $model['newsPerPage']; $i++) : ?>
    <?php if (!empty($model['allNews'][$i])) : ?>
        <div class="news-record border m-2">
            <h3 class="p-2"><?= $model['allNews'][$i]['title'] ?></h3>
            <div class="photo">
                <?php if (is_file('files/news/' . $model['allNews'][$i]['photo'] . '_s.jpg')) : ?>
                    <img class="bd-placeholder-img rounded float-start"
                         src="/files/news/<?= $model['allNews'][$i]['photo'] ?>_s.jpg"
                         alt="photo">
                <?php else : ?>
                    <svg class="bd-placeholder-img rounded float-start" width="200" height="200"
                         xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: 200x200"
                         preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title>
                        <rect width="100%" height="100%" fill="#868e96"></rect>
                    </svg>
                <?php endif; ?>
            </div>
            <div>
                <?= $model['allNews'][$i]['short_description'] ?>
            </div>
            <div class="news-buttons">
                <a href="/news/view?id=<?= $model['allNews'][$i]['id'] ?>" class="btn btn-primary">Читати далі</a>
                <?php if ($userModel->GetCurrentUser()['role'] == 'admin' || $userModel->GetCurrentUser()['id'] == $model['allNews'][$i]['user_id']) : ?>
                    <a href="/news/edit?id=<?= $model['allNews'][$i]['id'] ?>" class="btn btn-success">Редагувати</a>
                    <a href="/news/delete?id=<?= $model['allNews'][$i]['id'] ?>" class="btn btn-danger">Видалити</a>
                <?php endif; ?>
            </div>
            <div class="author">
                <p>
                    Автор: <?= $userModel->GetUserById($model['allNews'][$i]['user_id'])['nickname'] ?>
                </p>
            </div>
        </div>
    <?php endif; ?>
<?php endfor; ?>
<div style="text-align: center">
    <div class="pagination">
        <a href="/news?page=<?= $model['previousPage'] ?>&sort=<?= $model['sort'] ?>&author=<?= $model['currentAuthor'] ?>">&laquo;</a>
        <?php for ($i = 0; $i < $model['pagesCount']; $i++) : ?>
            <a href="/news?page=<?= $i ?>&sort=<?= $model['sort'] ?>&author=<?= $model['currentAuthor'] ?>"><?= $i + 1 ?></a>
        <?php endfor; ?>
        <a href="/news?page=<?= $model['nextPage'] ?>&sort=<?= $model['sort'] ?>&author=<?= $model['currentAuthor'] ?>">&raquo;</a>
    </div>
</div>