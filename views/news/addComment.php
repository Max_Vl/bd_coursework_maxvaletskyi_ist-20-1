<?php

?>
<div>
    <div class="form-group">
        <label for="newsTitle">Заголовок новини, для якої додається коментар:</label>
        <input type="text" name="newsTitle" value="<?=$model['news']['title'] ?>" class="form-control" readonly>
    </div>
    <div class="form-group">
        <label for="username">Ім'я користувача:</label>
        <input type="text" name="username" value="<?=$model['user']['nickname'] ?>" class="form-control" readonly>
    </div>
</div>
<form method="post" action="">
    <div class="form-group">
        <label for="text">Коментар:</label>
        <textarea name="text" type="text" class="form-control editor"
                  id="text">
        </textarea>
    </div>
    <input type="hidden" name="news_id" value="<?php echo $model['news_id']; ?>">
    <input type="hidden" name="user_id" value="<?php echo $model['user_id']; ?>">
    <button type="submit" class="btn btn-primary">Додати коментар</button>
</form>
