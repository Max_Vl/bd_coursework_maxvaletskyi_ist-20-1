<form action="" method="post" enctype="multipart/form-data">
    <div class="mb-3">
        <label for="title" class="form-label">Заголовок новини: </label>
        <input name="title" type="text" value="<?= $model['title'] ?>" class="form-control" id="title">
    </div>
    <div class="mb-3">
        <label for="short_description" class="form-label">Короткий опис новини: </label>
        <textarea name="short_description" type="text" class="form-control editor"
               id="short_description">
            <?= $model['short_description'] ?>
        </textarea>
    </div>
    <div class="mb-3">
        <label for="text" class="form-label">Текст новини: </label>
        <textarea name="text" type="text" class="form-control editor"
               id="text">
            <?= $model['text'] ?>
        </textarea>
    </div>
    <div class="mb-3">
        <label for="file" class="form-label">Фото: </label>
        <input type="file" name="file" accept="image/jpeg, image/png" id="file" class="form-control">
    </div>
    <div class="mb-3">
        <?php if(is_file('files/news/'.$model['photo'].'_b.jpg')) : ?>
            <img src="/files/news/<?=$model['photo']?>_b.jpg" alt="photo">
        <?php endif; ?>
    </div>
    <button type="submit" class="btn btn-primary">Зберегти</button>
</form>