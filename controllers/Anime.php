<?php

namespace controllers;

use core\Controller;

class Anime extends Controller
{
    protected $user;
    protected $animeModel;
    protected $userModel;
    protected $reviewModel;
    public function __construct()
    {
        $this->userModel = new \models\Users();
        $this->animeModel = new \models\Anime();
        $this->reviewModel = new \models\Reviews();
        $this->user = $this->userModel->GetCurrentUser();
    }

    public function actionIndex()
    {
        global $Config;
        $title = 'Аніме';
        $model = [];
        $genresIds = [];
        switch ($_GET['sort']) {
            case 'ASC':
                $model['sort'] = 'ASC';
                $model['sort_key'] = 'release_date';
                break;
            case 'DESC':
                $model['sort'] = 'DESC';
                $model['sort_key'] = 'release_date';
                break;
            case 'RatingASC':
                $model['sort'] = 'ASC';
                $model['sort_key'] = 'rating';
                break;
            case 'RatingDESC':
                $model['sort'] = 'DESC';
                $model['sort_key'] = 'rating';
                break;
        }
        $model['currentSort'] = $_GET['sort'];
        $model['anime'] = $this->animeModel->GetSortedAnime($model['sort_key'], $model['sort']);
        foreach ($model['anime'] as $singleAnime) {
            $genresArray = explode(', ', $singleAnime['genres']);
            foreach ($genresArray as $genreId) {
                if (!in_array($genreId, $genresIds))
                    $genresIds[] = $genreId;
            }
        }
        if ($_GET['genre'] != 'all') {
            $genreSpecificAnime = [];
            foreach ($model['anime'] as $anime) {
                $animeGenres = explode(', ', $anime['genres']);
                if (in_array($_GET['genre'], $animeGenres))
                    $genreSpecificAnime[] = $anime;
            }
            $model['anime'] = $genreSpecificAnime;
        }
        $model['pagesCount'] = ceil(count($model['anime']) / $Config['AnimaAndMangaCount']);
        $model['animePerPage'] = $Config['AnimaAndMangaCount'];
        $currentPage = $_GET['page'];
        $previousPage = $currentPage - 1;
        $nextPage = $currentPage + 1;
        if ($previousPage < 0)
            $previousPage = 0;
        if ($nextPage > $model['pagesCount'] - 1)
            $nextPage = $currentPage;
        $model['currentPage'] = $currentPage;
        $model['previousPage'] = $previousPage;
        $model['nextPage'] = $nextPage;
        $model['currentGenre'] = $_GET['genre'];
        $model['genres'] = $this->animeModel->GetGenres();
        return $this->render('index', ['model' => $model], [
            'MainTitle' => 'AniMangaDB | ' . $title,
            'PageTitle' => $title
        ]);
    }

    public function actionView()
    {
        $id = $_GET['id'];
        $anime = $this->animeModel->GetAnimeById($id);
        $title = $anime['title'];
        $anime['lastReview'] = $this->reviewModel->GetLastReviewByTypeAndId($id, "anime")[0];
        return $this->render('view', ['model' => $anime], [
            'MainTitle' => 'AniMangaDB | ' . $title,
            'PageTitle' => $title
        ]);
    }

    public function actionAdd()
    {
        $titleForbidden = 'Доступ заборонено';
        if ($this->user['role'] === "user")
            return $this->render('forbidden', null, [
                'MainTitle' => 'AniMangaDB | ' . $titleForbidden,
                'PageTitle' => $titleForbidden
            ]);
        $title = 'Додавання аніме';
        $genres = $this->animeModel->GetGenres();
        if ($this->isPost()) {
            $result = $this->animeModel->AddAnime($_POST);
            if ($result['error'] === false) {
                $allowedTypes = ['image/png', 'image/jpeg'];
                if (is_file($_FILES['file']['tmp_name']) && in_array($_FILES['file']['type'], $allowedTypes)) {
                    switch ($_FILES['file']['type']) {
                        case 'image/png':
                            $extension = 'png';
                            break;
                        default:
                            $extension = 'jpg';
                    }
                    $name = $result['id'] . '_' . uniqid() . '.' . $extension;
                    move_uploaded_file($_FILES['file']['tmp_name'], 'files/anime/' . $name);
                    $this->animeModel->changePhoto($result['id'], $name);
                }
                return $this->renderMessage('success', 'Аніме успішно додано', null, [
                    'MainTitle' => 'AniMangaDB | ' . $title,
                    'PageTitle' => $title
                ]);
            } else {
                $message = implode('<br>', $result['messages']);
                return $this->render('form', ['model' => $_POST, 'genres' => $genres], [
                    'PageTitle' => $title,
                    'MainTitle' => 'AniMangaDB | ' . $title,
                    'MessageText' => $message,
                    'MessageClass' => 'danger'
                ]);
            }
        } else
            return $this->render('form', ['genres' => $genres], [
                'MainTitle' => 'AniMangaDB | ' . $title,
                'PageTitle' => $title
            ]);
    }

    public function actionEdit()
    {
        $id = $_GET['id'];
        $anime = $this->animeModel->GetAnimeById($id);
        $genres = $this->animeModel->GetGenres();
        $titleForbidden = 'Доступ заборонено';
        if ($this->userModel->GetCurrentUser()['role'] === 'user')
            return $this->render('forbidden', null, [
                'MainTitle' => "AniMangaDB | " . $titleForbidden,
                'PageTitle' => $titleForbidden
            ]);
        $title = 'Редагування аніме';
        if ($this->isPost()) {
            $result = $this->animeModel->UpdateAnime($_POST, $id);
            if ($result === true) {
                $allowedTypes = ['image/png', 'image/jpeg'];
                if (is_file($_FILES['file']['tmp_name']) && in_array($_FILES['file']['type'], $allowedTypes)) {
                    switch ($_FILES['file']['type']) {
                        case 'image/png':
                            $extension = 'png';
                            break;
                        default:
                            $extension = 'jpg';
                    }
                    $name = $id . '_' . uniqid() . '.' . $extension;
                    move_uploaded_file($_FILES['file']['tmp_name'], 'files/anime/' . $name);
                    $this->animeModel->changePhoto($id, $name);
                    return $this->renderMessage('success', 'Аніме успішно збережено', null, [
                        'MainTitle' => "AniMangaDB | " . $title,
                        'PageTitle' => $title
                    ]);
                }
            } else {

                $message = implode('<br>', $result);
                return $this->render('form', ['model' => $anime, 'genres' => $genres], [
                    'PageTitle' => $title,
                    'MainTitle' => "AniMangaDB | " . $title,
                    'MessageText' => $message,
                    'MessageClass' => 'danger'
                ]);
            }
        } else
            return $this->render('form', ['model' => $anime, 'genres' => $genres], [
                'MainTitle' => "AniMangaDB | " . $title,
                'PageTitle' => $title
            ]);
    }

    public function actionDelete()
    {
        $title = 'Видалення аніме';
        $id = $_GET['id'];
        if (isset($_GET['confirm']) && $_GET['confirm'] == 'yes') {
            if ($this->animeModel->DeleteAnime($id))
                return $this->renderMessage('success', 'Аніме успішно видалено', null,
                    [
                        'PageTitle' => $title,
                        'MainTitle' => 'AniMangaDB | ' . $title
                    ]);
            else
                return $this->renderMessage('error', 'Помилка видалення аніме', null, [
                    'MainTitle' => "AniMangaDB | " . $title,
                    'PageTitle' => $title
                ]);
        }
        $anime = $this->animeModel->GetAnimeById($id);
        return $this->render('delete', ['model' => $anime], [
            'MainTitle' => "AniMangaDB | " . $title,
            'PageTitle' => $title
        ]);
    }
}