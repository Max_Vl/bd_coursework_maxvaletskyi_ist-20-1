<?php

namespace controllers;

use core\Controller;
use Couchbase\User;

class News extends Controller
{
    protected $user;
    protected $newsModel;
    protected $userModel;

    public function __construct()
    {
        $this->newsModel = new \models\News();
        $this->userModel = new \models\Users();
        $this->user = $this->userModel->GetCurrentUser();
    }

    public function actionIndex()
    {
        $title = 'Новини';
        global $Config;
        $model = [];
        $authors = [];
        if ($_GET['sort'] == 'ASC')
            $model['sort'] = 'ASC';
        else
            $model['sort'] = 'DESC';
        $model['allNews'] = \core\Core::getInstance()->getDB()->select('news', '*', null, ['datetime' => $model['sort']]);
        foreach ($model['allNews'] as $news) {
            if (!in_array($news['user_id'], $authors))
                $authors[] = $news['user_id'];
        }
        if ($_GET['author'] != 'all')
            $model['allNews'] = \core\Core::getInstance()->getDB()->select('news', '*', ['user_id' => $_GET['author']], ['datetime' => $model['sort']]);
        $model['pagesCount'] = ceil(count($model['allNews']) / $Config['NewsCount']);
        $model['newsPerPage'] = $Config['NewsCount'];
        $currentPage = $_GET['page'];
        $previousPage = $currentPage - 1;
        $nextPage = $currentPage + 1;
        if ($previousPage < 0)
            $previousPage = 0;
        if ($nextPage > $model['pagesCount'] - 1)
            $nextPage = $currentPage;
        $model['currentPage'] = $currentPage;
        $model['previousPage'] = $previousPage;
        $model['nextPage'] = $nextPage;
        $model['currentAuthor'] = $_GET['author'];
        return $this->render('index', ['model' => $model, 'authors' => $authors], [
            'MainTitle' => 'AniMangaDB | ' . $title,
            'PageTitle' => $title
        ]);
    }

    public function actionView()
    {
        $id = $_GET['id'];
        $news = $this->newsModel->GetNewsById($id);
        $news['reader_id'] = $this->user['id'];
        $news['lastComments'] = $this->newsModel->getLastCommentsByNewsId($id);
        $news['userModel'] = $this->userModel;
        return $this->render('view', ['model' => $news], [
            'MainTitle' => 'AniMangaDB | ' . $news['title'],
            'PageTitle' => $news['title']
        ]);
    }

    public function actionAdd()
    {
        $title = 'Додавання новини';
        $titleForbidden = 'Доступ заборонено';
        if (empty($this->user) || $this->user['role'] == 'user')
            return $this->render('forbidden', null, [
                'MainTitle' => 'AniMangaDB | ' . $titleForbidden,
                'PageTitle' => $titleForbidden
            ]);
        if ($this->isPost()) {
            $res = $this->newsModel->AddNews($_POST);
            if ($res['error'] === false) {
                $allowedTypes = ['image/png', 'image/jpeg'];
                if (is_file($_FILES['file']['tmp_name']) && in_array($_FILES['file']['type'], $allowedTypes)) {
                    switch ($_FILES['file']['type']) {
                        case 'image/png':
                            $extension = 'png';
                            break;
                        default:
                            $extension = 'jpg';
                            break;
                    }
                    $name = $res['id'] . '_' . uniqid() . '.' . $extension;
                    move_uploaded_file($_FILES['file']['tmp_name'], 'files/news/' . $name);
                    $this->newsModel->changePhoto($res['id'], $name);
                }
                return $this->renderMessage('success', 'Новину успішно додано', null,
                    [
                        'PageTitle' => $title,
                        'MainTitle' => 'AniMangaDB | ' . $title
                    ]);
            } else {
                $message = implode('<br>', $res['messages']);
                return $this->render('form', ['model' => $_POST], [
                    'PageTitle' => $title,
                    'MainTitle' => 'AniMangaDB | ' . $title,
                    'MessageText' => $message,
                    'MessageClass' => 'danger'
                ]);
            }
        } else
            return $this->render('form', ['model' => $_POST], [
                'MainTitle' => 'AniMangaDB | ' . $title,
                'PageTitle' => $title
            ]);
    }

    public function actionEdit()
    {
        $title = 'Редагування новини';
        $id = $_GET['id'];
        $news = $this->newsModel->GetNewsById($id);
        $titleForbidden = 'Доступ заборонено';
        if ($this->userModel->GetCurrentUser()['role'] != 'admin')
            if (empty($this->user) || $news['user_id'] != $this->userModel->GetCurrentUser()['id'] || $this->userModel->GetCurrentUser()['role'] == 'user')
                return $this->render('forbidden', null, [
                    'MainTitle' => 'AniMangaDB | ' . $titleForbidden,
                    'PageTitle' => $titleForbidden
                ]);
        if ($this->isPost()) {
            $res = $this->newsModel->UpdateNews($_POST, $id);
            if ($res === true) {
                $allowedTypes = ['image/png', 'image/jpeg'];
                if (is_file($_FILES['file']['tmp_name']) && in_array($_FILES['file']['type'], $allowedTypes)) {
                    switch ($_FILES['file']['type']) {
                        case 'image/png':
                            $extension = 'png';
                            break;
                        default:
                            $extension = 'jpg';
                            break;
                    }
                    $name = $id . '_' . uniqid() . '.' . $extension;
                    move_uploaded_file($_FILES['file']['tmp_name'], 'files/news/' . $name);
                    $this->newsModel->changePhoto($id, $name);
                }
                return $this->renderMessage('success', 'Новину успішно відредаговано', null,
                    [
                        'PageTitle' => $title,
                        'MainTitle' => 'AniMangaDB | ' . $title
                    ]);
            } else {
                $message = implode('<br>', $res);
                return $this->render('form', ['model' => $news], [
                    'PageTitle' => $title,
                    'MainTitle' => 'AniMangaDB | ' . $title,
                    'MessageText' => $message,
                    'MessageClass' => 'danger'
                ]);
            }
        } else
            return $this->render('form', ['model' => $news], [
                'MainTitle' => 'AniMangaDB | ' . $title,
                'PageTitle' => $title
            ]);
    }

    public function actionDelete()
    {
        $title = 'Видалення новини';
        $id = $_GET['id'];
        $news = $this->newsModel->GetNewsById($id);
        $titleForbidden = 'Доступ заборонено';
        if ($this->userModel->GetCurrentUser()['role'] != 'admin')
            if (empty($this->user) || $news['user_id'] != $this->userModel->GetCurrentUser()['id'] || $this->userModel->GetCurrentUser()['role'] == 'user')
                return $this->render('forbidden', null, [
                    'MainTitle' => 'AniMangaDB | ' . $titleForbidden,
                    'PageTitle' => $titleForbidden
                ]);
        if (isset($_GET['confirm']) && $_GET['confirm'] === 'yes') {
            if ($this->newsModel->DeleteNews($id))
                return $this->renderMessage('success', 'Новину успішно видалено', null,
                    [
                        'PageTitle' => $title,
                        'MainTitle' => 'AniMangaDB | ' . $title
                    ]);
            else
                return $this->renderMessage('error', 'Помилка видалення', null,
                    [
                        'PageTitle' => $title,
                        'MainTitle' => 'AniMangaDB | ' . $title
                    ]);
        }
        return $this->render('delete', ['model' => $news], [
            'MainTitle' => 'AniMangaDB | ' . $title,
            'PageTitle' => $title
        ]);
    }

    public function actionAddComment()
    {
        $title = 'Додавання коментарю';
        if ($_GET['user_id'] != $this->user['id']) {
            return $this->render('forbidden', null, [
                'PageTitle' => 'Доступ заборонено',
                'MainTitle' => 'AniMangaDB | Доступ заборонено'
            ]);
        }
        if ($this->isGet()) {
            $model['user_id'] = $_GET['user_id'];
            $model['news_id'] = $_GET['news_id'];
            $model['news'] = $this->newsModel->GetNewsById($model['news_id']);
            $model['user'] = $this->user;
            return $this->render('addComment', ['model' => $model], [
                'MainTitle' => 'AniMangaDB | ' . $title,
                'PageTitle' => $title
            ]);
        } elseif ($this->isPost()) {
            $result = $this->newsModel->addComment($_POST);
            if ($result === true)
                return $this->renderMessage('success', 'Коментар успішно додано', null,
                    [
                        'PageTitle' => $title,
                        'MainTitle' => 'AniMangaDB | ' . $title
                    ]);
            return $this->renderMessage('danger', 'Помилка додавання коментарю', null,
                [
                    'PageTitle' => $title,
                    'MainTitle' => 'AniMangaDB | ' . $title
                ]);
        }
        return $this->render('addComment', null, [
            'PageTitle' => $title,
            'MainTitle' => 'AniMangaDB | ' . $title
        ]);
    }
}