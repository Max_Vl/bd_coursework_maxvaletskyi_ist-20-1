<?php

namespace controllers;

use core\Controller;

class Site extends Controller
{
    protected $newsModel;
    protected $animeModel;
    protected $mangaModel;
    public function __construct()
    {
        $this->newsModel = new \models\News();
        $this->mangaModel = new \models\Manga();
        $this->animeModel = new \models\Anime();
    }
    public function actionIndex()
    {
        $model['lastNews'] = $this->newsModel->GetLastNews(3);
        $model['lastAnime'] = $this->animeModel->GetLastAnime(3);
        $model['lastManga'] = $this->mangaModel->GetLastManga(3);
        return $this->render('index', ['model' => $model], [
            'MainTitle' => 'AniMangaDB',
            'PageTitle' => ''
        ]);
    }
}