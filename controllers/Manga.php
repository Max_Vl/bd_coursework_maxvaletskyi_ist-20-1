<?php

namespace controllers;

use core\Controller;

class Manga extends Controller
{
    protected $user;
    protected $mangaModel;
    protected $userModel;

    public function __construct()
    {
        $this->userModel = new \models\Users();
        $this->mangaModel = new \models\Manga();
        $this->user = $this->userModel->GetCurrentUser();
    }

    public function actionIndex()
    {
        global $Config;
        $title = 'Манга';
        $model = [];
        $genresIds = [];
        switch ($_GET['sort']) {
            case 'ASC':
                $model['sort'] = 'ASC';
                $model['sort_key'] = 'release_date';
                break;
            case 'DESC':
                $model['sort'] = 'DESC';
                $model['sort_key'] = 'release_date';
                break;
        }
        $model['currentSort'] = $_GET['sort'];
        $model['manga'] = $this->mangaModel->GetSortedManga($model['sort_key'], $model['sort']);
        foreach ($model['manga'] as $singleManga) {
            $genresArray = explode(', ', $singleManga['genres']);
            foreach ($genresArray as $genreId) {
                if (!in_array($genreId, $genresIds))
                    $genresIds[] = $genreId;
            }
        }
        if ($_GET['genre'] != 'all') {
            $genreSpecificManga = [];
            foreach ($model['manga'] as $manga) {
                $mangaGenres = explode(', ', $manga['genres']);
                if (in_array($_GET['genre'], $mangaGenres))
                    $genreSpecificManga[] = $manga;
            }
            $model['manga'] = $genreSpecificManga;
        }
        $model['pagesCount'] = ceil(count($model['manga']) / $Config['AnimaAndMangaCount']);
        $currentPage = $_GET['page'];
        $previousPage = $currentPage - 1;
        $nextPage = $currentPage + 1;
        if ($previousPage < 0)
            $previousPage = 0;
        if ($nextPage > $model['pagesCount'] - 1)
            $nextPage = $currentPage;
        $model['currentPage'] = $currentPage;
        $model['previousPage'] = $previousPage;
        $model['nextPage'] = $nextPage;
        $model['currentGenre'] = $_GET['genre'];
        $model['genres'] = $this->mangaModel->GetGenres();
        return $this->render('index', ['model' => $model], [
            'MainTitle' => 'AniMangaDB | ' . $title,
            'PageTitle' => $title
        ]);
    }

    public function actionView()
    {
        $id = $_GET['id'];
        $manga = $this->mangaModel->GetMangaById($id);
        $title = $manga['title'];
        return $this->render('view', ['model' => $manga], [
            'MainTitle' => 'AniMangaDB | ' . $title,
            'PageTitle' => $title
        ]);
    }

    public function actionAdd()
    {
        $titleForbidden = 'Доступ заборонено';
        if ($this->user['role'] === "user")
            return $this->render('forbidden', null, [
                'MainTitle' => 'AniMangaDB | ' . $titleForbidden,
                'PageTitle' => $titleForbidden
            ]);
        $title = 'Додавання манги';
        $genres = $this->mangaModel->GetGenres();
        if ($this->isPost()) {
            $result = $this->mangaModel->AddManga($_POST);
            if ($result['error'] === false) {
                $allowedTypes = ['image/png', 'image/jpeg'];
                if (is_file($_FILES['file']['tmp_name']) && in_array($_FILES['file']['type'], $allowedTypes)) {
                    switch ($_FILES['file']['type']) {
                        case 'image/png':
                            $extension = 'png';
                            break;
                        default:
                            $extension = 'jpg';
                    }
                    $name = $result['id'] . '_' . uniqid() . '.' . $extension;
                    move_uploaded_file($_FILES['file']['tmp_name'], 'files/manga/' . $name);
                    $this->mangaModel->changePhoto($result['id'], $name);
                }
                return $this->renderMessage('success', 'Мангу успішно додано', null, [
                    'MainTitle' => 'AniMangaDB | ' . $title,
                    'PageTitle' => $title
                ]);
            } else {
                $message = implode('<br>', $result['messages']);
                return $this->render('form', ['model' => $_POST, 'genres' => $genres], [
                    'PageTitle' => $title,
                    'MainTitle' => 'AniMangaDB | ' . $title,
                    'MessageText' => $message,
                    'MessageClass' => 'danger'
                ]);
            }
        } else
            return $this->render('form', ['genres' => $genres], [
                'MainTitle' => 'AniMangaDB | ' . $title,
                'PageTitle' => $title
            ]);
    }

    public function actionEdit()
    {
        $id = $_GET['id'];
        $manga = $this->mangaModel->GetMangaById($id);
        $genres = $this->mangaModel->GetGenres();
        $titleForbidden = 'Доступ заборонено';
        if ($this->userModel->GetCurrentUser()['role'] === 'user')
            return $this->render('forbidden', null, [
                'MainTitle' => "AniMangaDB | " . $titleForbidden,
                'PageTitle' => $titleForbidden
            ]);
        $title = 'Редагування манги';
        if ($this->isPost()) {
            $result = $this->mangaModel->UpdateManga($_POST, $id);
            if ($result === true) {
                $allowedTypes = ['image/png', 'image/jpeg'];
                if (is_file($_FILES['file']['tmp_name']) && in_array($_FILES['file']['type'], $allowedTypes)) {
                    switch ($_FILES['file']['type']) {
                        case 'image/png':
                            $extension = 'png';
                            break;
                        default:
                            $extension = 'jpg';
                    }
                    $name = $id . '_' . uniqid() . '.' . $extension;
                    move_uploaded_file($_FILES['file']['tmp_name'], 'files/manga/' . $name);
                    $this->mangaModel->changePhoto($id, $name);
                    return $this->renderMessage('success', 'Мангу успішно збережено', null, [
                        'MainTitle' => "AniMangaDB | " . $title,
                        'PageTitle' => $title
                    ]);
                }
            } else {

                $message = implode('<br>', $result);
                return $this->render('form', ['model' => $manga, 'genres' => $genres], [
                    'PageTitle' => $title,
                    'MainTitle' => "AniMangaDB | " . $title,
                    'MessageText' => $message,
                    'MessageClass' => 'danger'
                ]);
            }
        } else
            return $this->render('form', ['model' => $manga, 'genres' => $genres], [
                'MainTitle' => "AniMangaDB | " . $title,
                'PageTitle' => $title
            ]);
    }

    public function actionDelete()
    {
        $title = 'Видалення манги';
        $id = $_GET['id'];
        if (isset($_GET['confirm']) && $_GET['confirm'] == 'yes') {
            if ($this->mangaModel->DeleteManga($id))
                return $this->renderMessage('success', 'Мангу успішно видалено', null,
                    [
                        'PageTitle' => $title,
                        'MainTitle' => 'AniMangaDB | ' . $title
                    ]);
            else
                return $this->renderMessage('error', 'Помилка видалення манги', null, [
                    'MainTitle' => "AniMangaDB | " . $title,
                    'PageTitle' => $title
                ]);
        }
        $manga = $this->mangaModel->GetMangaById($id);
        return $this->render('delete', ['model' => $manga], [
            'MainTitle' => "AniMangaDB | " . $title,
            'PageTitle' => $title
        ]);
    }
}