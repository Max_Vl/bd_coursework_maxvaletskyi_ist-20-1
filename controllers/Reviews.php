<?php

namespace controllers;

use core\Controller;

class Reviews extends Controller
{
    protected $user;
    protected $reviewsModel;
    protected $animeModel;
    protected $mangaModel;
    protected $userModel;
    public function __construct()
    {
        $this->userModel = new \models\Users();
        $this->reviewsModel = new \models\Reviews();
        $this->animeModel = new \models\Anime();
        $this->mangaModel = new \models\Manga();
        $this->user = $this->userModel->GetCurrentUser();
    }

    public function actionIndex()
    {
        global $Config;
        $reviewTarget = null;
        $id = $_GET['id'];
        $type = $_GET['type'];
        if ($type == 'anime')
            $reviewTarget = $this->animeModel->GetAnimeById($id);
        else
            $reviewTarget = $this->mangaModel->GetMangaById($id);
        $countReviews = $Config['ReviewsCount'];
        $reviews = $this->reviewsModel->GetReviewsByTargetIdAndType($id, $countReviews, $type);
        $model['reviewType'] = $type;
        $model['targetId'] = $id;
        $model['freshReviews'] = $reviews;
        $model['reviewTarget'] = $reviewTarget;
        $title = 'Відгуки';
        return $this->render('index', ['model' => $model], [
            'MainTitle' => "AniMangaDB | " . $title,
            'PageTitle' => $title
        ]);
    }

    public function actionAdd()
    {
        $titleForbidden = 'Доступ заборонено';
        if(empty($this->user))
            return $this->render('forbidden', null, [
                'MainTitle' => "AniMangaDB | " . $titleForbidden,
                'PageTitle' => $titleForbidden
            ]);
        $title = 'Додавання відгуку';
        if ($this->isPost())
        {
            $reviewType = $_GET['type'];
            $id = $_GET['targetId'];
            $result = $this->reviewsModel->AddReview($_POST, $reviewType, $id);
            if($result['error'] === false)
            {
                return $this->renderMessage('success', 'Відгук успішно додано', null, [
                    'MainTitle' => $title,
                    'PageTitle' => $title
                ]);
            } else
            {
                $message = implode('<br>', $result['messages']);
                return $this->render('form', ['model' => $_POST], [
                    'PageTitle' => $title,
                    'MainTitle' => $title,
                    'MessageText' => $message,
                    'MessageClass' => 'danger'
                ]);
            }
        } else
            return $this->render('form', null, [
                'MainTitle' => $title,
                'PageTitle' => $title
            ]);
    }

    public function actionView()
    {
        $id = $_GET['id'];
        $review = $this->reviewsModel->GetReviewById($id);
        $marks = $this->reviewsModel->GetMarksByReviewId($review['id']);
        $title = 'Відгук від користувача '.$this->userModel->GetUserById($review['user_id'])['nickname'];
        return $this->render('view', ['model' => $review, 'marks' => $marks], [
            'MainTitle' => "AniMangaDB | " . $title,
            'PageTitle' => $title
        ]);
    }

    public function actionEdit()
    {
        $id = $_GET['id'];
        $review = $this->reviewsModel->GetReviewById($id);
        $marks = $this->reviewsModel->GetMarksByReviewId($id);
        $titleForbidden = 'Доступ заборонено';
        if($this->userModel->GetCurrentUser()['access'] != '1')
        {
            if (empty($this->user) || $review['user_id'] != $this->userModel->GetCurrentUser()['id'])
                return $this->render('forbidden', null, [
                    'MainTitle' => $titleForbidden,
                    'PageTitle' => $titleForbidden
                ]);
        }
        $title = 'Редагування відгуку';
        if ($this->isPost())
        {
            $result = $this->reviewsModel->EditReview($_POST, $id);
            if($result === true)
            {
                return $this->renderMessage('success', 'Відгук успішно збережено', null, [
                    'MainTitle' => "AniMangaDB" . $title,
                    'PageTitle' => $title
                ]);
            } else
            {
                $message = implode('<br>', $result);
                return $this->render('form', ['model' => $review, 'marks' => $marks], [
                    'PageTitle' => $title,
                    'MainTitle' => $title,
                    'MessageText' => $message,
                    'MessageClass' => 'danger'
                ]);
            }
        } else
            return $this->render('form', ['model' => $review, 'marks' => $marks], [
                'MainTitle' => $title,
                'PageTitle' => $title
            ]);
    }

    public function actionDelete()
    {
        $title = 'Видалення відгуку';
        $id = $_GET['id'];
        if(isset($_GET['confirm']) && $_GET['confirm'] == 'yes')
        {
            if($this->reviewsModel->DeleteReview($id))
                return $this->renderMessage('success', 'Відгук успішно видалено', null, [
                    'MainTitle' => "AniMangaDB | " . $title,
                    'PageTitle' => $title
                ]);
            else
                return $this->renderMessage('error', 'Помилка видалення відгуку', null, [
                    'MainTitle' => "AniMangaDB | " . $title,
                    'PageTitle' => $title
                ]);

        }
        $review = $this->reviewsModel->GetReviewById($id);
        return $this->render('delete', ['model' => $review], [
            'MainTitle' => "AniMangaDB | " . $title,
            'PageTitle' => $title
        ]);
    }
}