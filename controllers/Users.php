<?php

namespace controllers;

use core\Controller;

class Users extends Controller
{
    protected $usersModel;

    function __construct()
    {
        $this->usersModel = new \models\Users();
    }
    public function actionLogout()
    {
        $title = 'Вихід';
        unset($_SESSION['user']);
        return $this->renderMessage('success', 'Вихід успішний', null,
            [
                'PageTitle' => $title,
                'MainTitle' => 'AniMangaDB | '.$title
            ]);
    }
    public function actionLogin()
    {
        $title = 'Вхід';
        if (isset($_SESSION['user']))
            return $this->renderMessage('success', 'Ви вже увійшли на сайт', null,
                [
                    'PageTitle' => $title,
                    'MainTitle' => 'AniMangaDB | '.$title
                ]);
        if ($this->isPost()) {
            $user = $this->usersModel->AuthUser($_POST['login'], $_POST['password']);
            if (!empty($user)) {
                $_SESSION['user'] = $user;
                return $this->renderMessage('success', 'Вхід успішний', null,
                    [
                        'PageTitle' => $title,
                        'MainTitle' => 'AniMangaDB | '.$title
                    ]);
            }
            else {
                return $this->render('login', null, [
                    'PageTitle' => $title,
                    'MainTitle' => 'AniMangaDB | '.$title,
                    'MessageText' => 'Неправильний логін або пароль',
                    'MessageClass' => 'danger'
                ]);
            }
        } else {
            return $this->render('login', null, [
                'PageTitle' => $title,
                'MainTitle' => 'AniMangaDB | '.$title
            ]);
        }
    }
    public function actionRegistration()
    {
        $title = 'Реєстрація';
        if ($this->isPost()) {
            $res = $this->usersModel->AddUser($_POST);
            if ($res === true)
                return $this->renderMessage('success', 'Користувач успішно зареєстрований', null,
                    [
                        'PageTitle' => $title,
                        'MainTitle' => 'AniMangaDB | '.$title
                    ]);
            else {
                $message = implode('<br>', $res);
                return $this->render('registration', null, [
                    'PageTitle' => $title,
                    'MainTitle' => 'AniMangaDB | '.$title,
                    'MessageText' => $message,
                    'MessageClass' => 'danger'
                ]);
            }
        } else {
            return $this->render('registration', null, [
                'PageTitle' => $title,
                'MainTitle' => 'AniMangaDB | '.$title
            ]);
        }
    }
    public function actionProfile()
    {
        $title = 'Перегляд профілю';
        return $this->render('profile', null, [
            'PageTitle' => $title,
            'MainTitle' => 'AniMangaDB | '.$title
        ]);
    }
    public function actionEdit()
    {
        $title = 'Редагування профілю';
        $id = $_GET['user_id'];
        $user = $this->usersModel->GetUserById($id);
        $titleForbidden = 'Доступ заборонено';
        if (empty($user) || $id != $this->usersModel->GetCurrentUser()['id'])
            return $this->render('forbidden', null, [
                'MainTitle' => 'AniMangaDB | ' . $titleForbidden,
                'PageTitle' => $titleForbidden
            ]);
        if ($this->isPost()) {
            $res = $this->usersModel->UpdateUser($_POST, $id);
            if ($res === true) {
                return $this->renderMessage('success', 'Дані користувача успішно відредаговано', null,
                    [
                        'PageTitle' => $title,
                        'MainTitle' => 'AniMangaDB | ' . $title
                    ]);
            }
            else {
                $message = implode('<br>', $res);
                return $this->render('edit', ['model' => $user], [
                    'PageTitle' => $title,
                    'MainTitle' => 'AniMangaDB | ' . $title,
                    'MessageText' => $message,
                    'MessageClass' => 'danger'
                ]);
            }
        } else
            return $this->render('edit', ['model' => $user], [
                'MainTitle' => 'AniMangaDB | ' . $title,
                'PageTitle' => $title
            ]);
    }
    public function actionEditPassword()
    {
        $title = 'Зміна паролю';
        $id = $_GET['user_id'];
        $user = $this->usersModel->GetUserById($id);
        $titleForbidden = 'Доступ заборонено';
        if (empty($user) || $id != $this->usersModel->GetCurrentUser()['id'])
            return $this->render('forbidden', null, [
                'MainTitle' => 'AniMangaDB | ' . $titleForbidden,
                'PageTitle' => $titleForbidden
            ]);
        if ($this->isPost()) {
            $res = $this->usersModel->ChangePassword($_POST, $id);
            if ($res === true) {
                return $this->renderMessage('success', 'Пароль успішно змінено', null,
                    [
                        'PageTitle' => $title,
                        'MainTitle' => 'AniMangaDB | ' . $title
                    ]);
            }
            else {
                $message = implode('<br>', $res);
                return $this->render('editPassword', null, [
                    'PageTitle' => $title,
                    'MainTitle' => 'AniMangaDB | ' . $title,
                    'MessageText' => $message,
                    'MessageClass' => 'danger'
                ]);
            }
        } else
            return $this->render('editPassword', null, [
                'MainTitle' => 'AniMangaDB | ' . $title,
                'PageTitle' => $title
            ]);
    }
}