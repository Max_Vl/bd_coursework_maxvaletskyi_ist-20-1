<?php

namespace models;

use core\Core;
use core\Model;
use core\Utils;

class Anime extends Model
{
    public function changePhoto($id, $file)
    {
        $folder = 'files/anime/';
        $file_path = pathinfo($folder . $file);
        $file_big = $file_path['filename'] . '_b';
        $file_middle = $file_path['filename'] . '_m';
        $file_small = $file_path['filename'] . '_s';
        $anime = $this->GetAnimeById($id);
        if (is_file($folder . $anime['photo'] . '_b.jpg') && is_file($folder . $file)) {
            unlink($folder . $anime['photo'] . '_b.jpg');
        }
        if (is_file($folder . $anime['photo'] . '_m.jpg') && is_file($folder . $file)) {
            unlink($folder . $anime['photo'] . '_m.jpg');
        }
        if (is_file($folder . $anime['photo'] . '_s.jpg') && is_file($folder . $file)) {
            unlink($folder . $anime['photo'] . '_s.jpg');
        }
        $anime['photo'] = $file_path['filename'];
        $im_b = new \Imagick();
        $im_b->readImage($_SERVER['DOCUMENT_ROOT'] . '/' . $folder . $file);
        $im_b->cropThumbnailImage(1120, 1600, true);
        $im_b->writeImage($_SERVER['DOCUMENT_ROOT'] . '/' . $folder . '/' . $file_big . '.jpg');
        $this->UpdateAnime($anime, $id);
        $im_m = new \Imagick();
        $im_m->readImage($_SERVER['DOCUMENT_ROOT'] . '/' . $folder . $file);
        $im_m->cropThumbnailImage(560, 800, true);
        $im_m->writeImage($_SERVER['DOCUMENT_ROOT'] . '/' . $folder . '/' . $file_middle . '.jpg');
        $this->UpdateAnime($anime, $id);
        $im_s = new \Imagick();
        $im_s->readImage($_SERVER['DOCUMENT_ROOT'] . '/' . $folder . $file);
        $im_s->cropThumbnailImage(280, 400, true);
        $im_s->writeImage($_SERVER['DOCUMENT_ROOT'] . '/' . $folder . '/' . $file_small . '.jpg');
        unlink($folder . $file);
        $this->UpdateAnime($anime, $id);
    }

    public function AddAnime($row)
    {
        $userModel = new Users();
        $user = $userModel->GetCurrentUser();
        if ($user['role'] === "admin" || $user['role'] === "editor") {

            $fields = ['title', 'type', 'episodes_count', 'genres', 'status', 'rating', 'description',
                'studio_name', 'release_date'];
            $rowFiltered = Utils::ArrayFilter($row, $fields);
            $rowFiltered['photo'] = '...photo...';
            if (is_array($row['genres'])) {
                $rowFiltered['genres'] = implode(', ', $row['genres']);
            }
            $id = \core\Core::getInstance()->getDB()->insert('anime', $rowFiltered);
            return [
                'error' => false,
                'id' => $id
            ];
        }
        return [
            'error' => true,
            'messages' => ['Користувач не має необхідних прав доступу']
        ];
    }

    public function GetLastAnime($count)
    {
        return \core\Core::getInstance()->getDB()->select('anime', '*', null, null, $count);
    }

    public function GetAllAnime()
    {
        return \core\Core::getInstance()->getDB()->select('anime');
    }

    public function GetAnimeById($id)
    {
        $news = \core\Core::getInstance()->getDB()->select('anime', '*', ['id' => $id]);
        if (!empty($news))
            return $news[0];
        else
            return null;
    }

    public function UpdateAnime($row, $id)
    {
        $fields = ['title', 'type', 'episodes_count', 'genres', 'status', 'rating', 'description',
            'studio_name', 'release_date', 'photo'];
        $rowFiltered = Utils::ArrayFilter($row, $fields);
        if (is_array($row['genres'])) {
            $rowFiltered['genres'] = implode(', ', $row['genres']);
        }
        \core\Core::getInstance()->getDB()->update('anime', $rowFiltered, ['id' => $id]);
        return true;
    }

    public function DeleteAnime($id)
    {
        $anime = $this->GetAnimeById($id);
        if (!empty($anime)) {
            unlink('files/anime/'.$anime['photo']."_b.jpg");
            unlink('files/anime/'.$anime['photo']."_m.jpg");
            unlink('files/anime/'.$anime['photo']."_s.jpg");
            \core\Core::getInstance()->getDB()->delete('anime', ['id' => $id]);
            return true;
        } else
            return false;
    }

    public function GetGenres()
    {
        return \core\Core::getInstance()->getDB()->select("genres");
    }
    public function GetGenreById($id)
    {
        return \core\Core::getInstance()->getDB()->select("genres", '*', ['id' => $id]);
    }
    public function GetSortedAnime($sort_key, $sort)
    {
        return \core\Core::getInstance()->getDB()->select('anime', '*', null, [$sort_key => $sort]);
    }
}