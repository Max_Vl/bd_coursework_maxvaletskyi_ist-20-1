<?php

namespace models;

use core\Model;
use core\Utils;

class Users extends Model
{
    public function Validate($formRow)
    {
        $errors = [];
        if (!empty($this->GetUserByLogin($formRow['login'])))
            $errors [] = "Користувач з таким логіном вже існує";
        if (!empty($this->GetUserByNickname($formRow['nickname'])))
            $errors [] = "Користувач з таким нікнеймом вже існує";
        if (empty($formRow['login']))
            $errors [] = 'Логін не може бути порожнім';
        if (empty($formRow['password']))
            $errors [] = 'Пароль не може бути порожнім';
        if ($formRow['password'] != $formRow['password2'])
            $errors [] = 'Паролі не співпадають';
        if (empty($formRow['nickname']))
            $errors [] = 'Нікнейм не може бути порожнім';
        if (count($errors) > 0) {
            return $errors;
        } else
            return true;
    }
    public function ValidateUpdate($userData, $formRow)
    {
        $errors = [];
        if($userData['login'] != $formRow['login']) {
            if (!empty($this->GetUserByLogin($formRow['login'])))
                $errors [] = "Користувач з таким логіном вже існує";
        }
        if($userData['nickname'] != $formRow['nickname']) {
            if (!empty($this->GetUserByNickname($formRow['nickname'])))
                $errors [] = "Користувач з таким нікнеймом вже існує";
        }
        if (empty($formRow['login']))
            $errors [] = 'Логін не може бути порожнім';
        if (empty($formRow['nickname']))
            $errors [] = 'Нікнейм не може бути порожнім';
        if (count($errors) > 0) {
            return $errors;
        } else
            return true;
    }
    public function ValidatePassword($userData, $formRow)
    {
        $errors = [];
        if (md5($formRow['oldPassword']) != $userData['password']) {
            $errors [] = 'Неправильний пароль';
            return $errors;
        } else {
            if (empty($formRow['password']))
                $errors [] = 'Пароль не може бути порожнім';
            if ($formRow['password'] != $formRow['password2'])
                $errors [] = 'Паролі не співпадають';
        }
        if (count($errors) > 0) {
            return $errors;
        } else
            return true;
    }
    public function isUserAuthenticated()
    {
        return isset($_SESSION['user']);
    }

    public function GetCurrentUser()
    {
        if ($this->isUserAuthenticated())
            return $_SESSION['user'];
        else
            return null;
    }

    public function AddUser($userRow)
    {
        $validationResult = $this->Validate($userRow);
        if (is_array($validationResult))
            return $validationResult;
        $fields = ['login', 'password', 'nickname'];
        $userRowFiltered = Utils::ArrayFilter($userRow, $fields);
        $userRowFiltered['password'] = md5($userRow['password']);
        \core\Core::getInstance()->getDB()->insert('users', $userRowFiltered);
        return true;
    }
    public function UpdateUser($userRow, $id)
    {
        $validationResult = $this->ValidateUpdate($this->GetUserById($id), $userRow);
        if(is_array($validationResult))
            return $validationResult;
        $fields = ['login', 'nickname'];
        $userRowFiltered = Utils::ArrayFilter($userRow, $fields);
        \core\Core::getInstance()->getDB()->update('users', $userRowFiltered, ['id' => $id]);
        return true;
    }
    public function ChangePassword($userRow, $id)
    {
        $validationResult = $this->ValidatePassword($this->GetUserById($id), $userRow);
        if(is_array($validationResult))
            return $validationResult;
        $fields = ['password'];
        $userRowFiltered = Utils::ArrayFilter($userRow, $fields);
        $userRowFiltered['password'] = md5($userRow['password']);
        \core\Core::getInstance()->getDB()->update('users', $userRowFiltered, ['id' => $id]);
        return true;
    }
    public function AuthUser($login, $password)
    {
        $password = md5($password);
        $users = \core\Core::getInstance()->getDB()->select('users', "*",
            [
                'login' => $login,
                'password' => $password
            ]);
        if (count($users) > 0) {
            return $users[0];
        } else
            return false;
    }

    public function GetUserByLogin($login)
    {
        $rows = \core\Core::getInstance()->getDB()->select('users', '*',
            ['login' => $login]);
        if (count($rows) > 0)
            return $rows[0];
        else
            return null;
    }

    public function GetUserByNickname($nickname)
    {
        $rows = \core\Core::getInstance()->getDB()->select('users', '*',
            ['nickname' => $nickname]);
        if (count($rows) > 0)
            return $rows[0];
        else
            return null;
    }
    public function GetUserById($id)
    {
        $rows = \core\Core::getInstance()->getDB()->select('users', '*',
            ['id' => $id]);
        if (count($rows) > 0)
            return $rows[0];
        else
            return null;
    }
}