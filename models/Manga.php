<?php

namespace models;

use core\Model;
use core\Utils;

class Manga extends Model
{
    public function changePhoto($id, $file)
    {
        $folder = 'files/manga/';
        $file_path = pathinfo($folder . $file);
        $file_big = $file_path['filename'] . '_b';
        $file_middle = $file_path['filename'] . '_m';
        $file_small = $file_path['filename'] . '_s';
        $manga = $this->GetMangaById($id);
        if (is_file($folder . $manga['photo'] . '_b.jpg') && is_file($folder . $file)) {
            unlink($folder . $manga['photo'] . '_b.jpg');
        }
        if (is_file($folder . $manga['photo'] . '_m.jpg') && is_file($folder . $file)) {
            unlink($folder . $manga['photo'] . '_m.jpg');
        }
        if (is_file($folder . $manga['photo'] . '_s.jpg') && is_file($folder . $file)) {
            unlink($folder . $manga['photo'] . '_s.jpg');
        }
        $manga['photo'] = $file_path['filename'];
        $im_b = new \Imagick();
        $im_b->readImage($_SERVER['DOCUMENT_ROOT'] . '/' . $folder . $file);
        $im_b->cropThumbnailImage(1120, 1600, true);
        $im_b->writeImage($_SERVER['DOCUMENT_ROOT'] . '/' . $folder . '/' . $file_big . '.jpg');
        $this->UpdateManga($manga, $id);
        $im_m = new \Imagick();
        $im_m->readImage($_SERVER['DOCUMENT_ROOT'] . '/' . $folder . $file);
        $im_m->cropThumbnailImage(560, 800, true);
        $im_m->writeImage($_SERVER['DOCUMENT_ROOT'] . '/' . $folder . '/' . $file_middle . '.jpg');
        $this->UpdateManga($manga, $id);
        $im_s = new \Imagick();
        $im_s->readImage($_SERVER['DOCUMENT_ROOT'] . '/' . $folder . $file);
        $im_s->cropThumbnailImage(280, 400, true);
        $im_s->writeImage($_SERVER['DOCUMENT_ROOT'] . '/' . $folder . '/' . $file_small . '.jpg');
        unlink($folder . $file);
        $this->UpdateManga($manga, $id);
    }

    public function AddManga($row)
    {
        $userModel = new Users();
        $user = $userModel->GetCurrentUser();
        if ($user['role'] === "admin" || $user['role'] === "editor") {
            $fields = ['title', 'genres', 'chapters_count', 'status', 'description', 'author', 'release_date'];
            $rowFiltered = Utils::ArrayFilter($row, $fields);
            $rowFiltered['photo'] = '...photo...';
            if (is_array($row['genres'])) {
                $rowFiltered['genres'] = implode(', ', $row['genres']);
            }
            $id = \core\Core::getInstance()->getDB()->insert('manga', $rowFiltered);
            return [
                'error' => false,
                'id' => $id
            ];
        }
        return [
            'error' => true,
            'messages' => ['Користувач не має необхідних прав доступу']
        ];
    }

    public function GetLastManga($count)
    {
        return \core\Core::getInstance()->getDB()->select('manga', '*', null, null, $count);
    }

    public function GetAllManga()
    {
        return \core\Core::getInstance()->getDB()->select('manga');
    }

    public function GetMangaById($id)
    {
        $manga = \core\Core::getInstance()->getDB()->select('manga', '*', ['id' => $id]);
        if (!empty($manga))
            return $manga[0];
        else
            return null;
    }

    public function UpdateManga($row, $id)
    {
        $fields = ['title', 'genres', 'chapters_count', 'status', 'description', 'author', 'release_date', 'photo'];
        $rowFiltered = Utils::ArrayFilter($row, $fields);
        if (is_array($row['genres'])) {
            $rowFiltered['genres'] = implode(', ', $row['genres']);
        }
        \core\Core::getInstance()->getDB()->update('manga', $rowFiltered, ['id' => $id]);
        return true;
    }

    public function DeleteManga($id)
    {
        $manga = $this->GetMangaById($id);
        if (!empty($manga)) {
            unlink('files/manga/' . $manga['photo'] . "_b.jpg");
            unlink('files/manga/' . $manga['photo'] . "_m.jpg");
            unlink('files/manga/' . $manga['photo'] . "_s.jpg");
            \core\Core::getInstance()->getDB()->delete('manga', ['id' => $id]);
            return true;
        } else
            return false;
    }

    public function GetGenres()
    {
        return \core\Core::getInstance()->getDB()->select("genres");
    }

    public function GetGenreById($id)
    {
        return \core\Core::getInstance()->getDB()->select("genres", '*', ['id' => $id]);
    }
    public function GetSortedManga($sort_key, $sort)
    {
        return \core\Core::getInstance()->getDB()->select('manga', '*', null, [$sort_key => $sort]);
    }
}