<?php

namespace models;

use core\Model;
use core\Utils;

class Reviews extends Model
{
    public function AddReview($row, $reviewType, $id)
    {
        $userModel = new \models\Users();
        $user = $userModel->GetCurrentUser();
        if($user == null)
            return [
                'error' => true,
                'messages' => ['Користувач не аутентифікований']
            ];
        $validationResult = $this->validation($row);
        if (is_array($validationResult))
            return [
                'error' => true,
                'messages' => $validationResult
            ];
        $fieldsReview = ['short_text', 'text'];
        $filteredRowReview = Utils::arrayFilter($row, $fieldsReview);
        $filteredRowReview['datetime'] = date('Y-m-d H:i:s');
        $filteredRowReview['target_id'] = $id;
        $filteredRowReview['user_id'] = $user['id'];
        $filteredRowReview['review_type'] = $reviewType;
        $reviewId = \core\Core::getInstance()->getDB()->insert('reviews', $filteredRowReview);

        $fieldsMarks = ['story', 'drama', 'characters', 'atmosphere'];
        $filteredRowMarks = Utils::arrayFilter($row, $fieldsMarks);
        $filteredRowMarks['id_review'] = $reviewId;
        \core\Core::getInstance()->getDB()->insert('marks', $filteredRowMarks);
        return [
            'error' => false,
            'id' => $id
        ];
    }

    public function EditReview($review, $id)
    {
        $userModel = new \models\Users();
        $user = $userModel->GetCurrentUser();
        if($user == null)
            return false;
        $validationResult = $this->Validation($review);
        if (is_array($validationResult))
            return $validationResult;
        $fields = ['short_text', 'text'];
        $filteredRow = Utils::ArrayFilter($review, $fields);
        \core\Core::getInstance()->getDB()->update('reviews', $filteredRow, ['id' => $id]);
        $fieldsMarks = ['story', 'drama', 'characters', 'atmosphere'];
        $filteredRowMarks = Utils::arrayFilter($review, $fieldsMarks);
        \core\Core::getInstance()->getDB()->update('marks', $filteredRowMarks, ['id_review' => $id]);
        return true;
    }

    public function DeleteReview($id)
    {
        $review = $this->GetReviewById($id);
        $userModel = new \models\Users();
        $user = $userModel->GetCurrentUser();
        if($user['access'] !== '1')
        {
            if (empty($review) || empty($user) || $user == null || $user['id'] != $review['user_id'])
                return false;
        }
        \core\Core::getInstance()->getDB()->delete('marks', ['id_review' => $review['id']]);
        \core\Core::getInstance()->getDB()->delete('reviews', ['id' => $id]);
        return true;
    }

    public function Validation($row)
    {
        $arrayOfErrors = [];

        if (empty($row['short_text']))
            $arrayOfErrors [] = 'Поле "Короткий текст відгуку" не може бути порожнім!';
        if (empty($row['text']))
            $arrayOfErrors [] = 'Поле "Повний текст відгуку" не може бути порожнім!';

        if(count($arrayOfErrors) > 0)
            return $arrayOfErrors;
        else
            return true;
    }
    public function GetReviewsByTargetIdAndType($id, $count, $type)
    {
        return \core\Core::getInstance()->getDB()->select('reviews', '*', ['target_id' => $id,
            'review_type' => $type], ['datetime' => 'DESC'], $count);
    }
    public function GetLastReviewByTypeAndId($id, $type)
    {
        return \core\Core::getInstance()->getDB()->select('reviews', '*', ['target_id' => $id,
            'review_type' => $type], ['datetime' => 'DESC'], 1);
    }
    public function GetReviewById($id)
    {
        $reviews = \core\Core::getInstance()->getDB()->select('reviews', '*', ['id' => $id]);
        if(!empty($reviews))
            return $reviews[0];
        else
            return null;
    }
    public function GetLastReviewByUserId($id)
    {
        return \core\Core::getInstance()->getDB()->select('reviews', '*', ['user_id' => $id], ['datetime' => 'DESC'], 1)[0];
    }
    public function GetAllReviewsByUserId($id)
    {
        return \core\Core::getInstance()->getDB()->select('reviews', '*', ['user_id' => $id], ['datetime' => 'DESC']);
    }
    public function GetMarksByReviewId($id)
    {
        return \core\Core::getInstance()->getDB()->select('marks', '*', ['id_review'  => $id])[0];
    }
}