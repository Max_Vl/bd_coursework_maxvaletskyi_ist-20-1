<?php

namespace models;

use core\Model;
use core\Utils;

class News extends Model
{
    public function changePhoto($id, $file)
    {
        $folder = 'files/news/';
        $file_path = pathinfo($folder . $file);
        $file_big = $file_path['filename'] . '_b';
        $file_middle = $file_path['filename'] . '_m';
        $file_small = $file_path['filename'] . '_s';
        $news = $this->GetNewsById($id);
        if (is_file($folder . $news['photo'] . '_b.jpg') && is_file($folder . $file)) {
            unlink($folder . $news['photo'] . '_b.jpg');
        }
        if (is_file($folder . $news['photo'] . '_m.jpg') && is_file($folder . $file)) {
            unlink($folder . $news['photo'] . '_m.jpg');
        }
        if (is_file($folder . $news['photo'] . '_s.jpg') && is_file($folder . $file)) {
            unlink($folder . $news['photo'] . '_s.jpg');
        }
        $news['photo'] = $file_path['filename'];
        $im_b = new \Imagick();
        $im_b->readImage($_SERVER['DOCUMENT_ROOT'] . '/' . $folder . $file);
        $im_b->cropThumbnailImage(1280, 1024, true);
        $im_b->writeImage($_SERVER['DOCUMENT_ROOT'] . '/' . $folder . '/' . $file_big . '.jpg');
        $this->UpdateNews($news, $id);
        $im_m = new \Imagick();
        $im_m->readImage($_SERVER['DOCUMENT_ROOT'] . '/' . $folder . $file);
        $im_m->cropThumbnailImage(300, 200, true);
        $im_m->writeImage($_SERVER['DOCUMENT_ROOT'] . '/' . $folder . '/' . $file_middle . '.jpg');
        $this->UpdateNews($news, $id);
        $im_s = new \Imagick();
        $im_s->readImage($_SERVER['DOCUMENT_ROOT'] . '/' . $folder . $file);
        $im_s->cropThumbnailImage(200, 200, true);
        $im_s->writeImage($_SERVER['DOCUMENT_ROOT'] . '/' . $folder . '/' . $file_small . '.jpg');
        unlink($folder . $file);
        $this->UpdateNews($news, $id);
    }

    public function AddNews($row)
    {
        $userModel = new Users();
        $user = $userModel->GetCurrentUser();
        if ($user == null) {
            return [
                'error' => true,
                'messages' => ['Користувач не автентифікований']
            ];
        }
        $validationResult = $this->Validate($row);
        if (is_array($validationResult)) {
            return [
                'error' => true,
                'messages' => $validationResult
            ];
        }
        $fields = ['title', 'short_description', 'text'];
        $rowFiltered = Utils::ArrayFilter($row, $fields);
        $rowFiltered['datetime'] = date('Y-m-d H:i:s');
        $rowFiltered['user_id'] = $user['id'];
        $rowFiltered['photo'] = '...photo...';
        $id = \core\Core::getInstance()->getDB()->insert('news', $rowFiltered);
        return [
            'error' => false,
            'id' => $id
        ];
    }

    public function GetLastNews($count)
    {
        return \core\Core::getInstance()->getDB()->select('news', '*', null, ['datetime' => 'DESC'], $count);
    }

    public function GetNewsById($id)
    {
        $news = \core\Core::getInstance()->getDB()->select('news', '*', ['id' => $id]);
        if (!empty($news))
            return $news[0];
        else
            return null;
    }

    public function UpdateNews($row, $id)
    {
        $validationResult = $this->Validate($row);
        $fields = ['title', 'short_description', 'text', 'photo'];
        $rowFiltered = Utils::ArrayFilter($row, $fields);
        $rowFiltered['datetime_lastEdit'] = date('Y-m-d H:i:s');
        \core\Core::getInstance()->getDB()->update('news', $rowFiltered, ['id' => $id]);
        return $validationResult;
    }

    public function DeleteNews($id)
    {
        $news = $this->GetNewsById($id);
        if (!empty($news)) {
            unlink('files/news/'.$news['photo']."_b.jpg");
            unlink('files/news/'.$news['photo']."_m.jpg");
            unlink('files/news/'.$news['photo']."_s.jpg");
            \core\Core::getInstance()->getDB()->delete('news', ['id' => $id]);
            return true;
        } else
            return false;
    }

    public function Validate($row)
    {
        $errors = [];
        if (empty($row['title']))
            $errors [] = 'Заголовок не може бути порожнім';
        if (empty($row['short_description']))
            $errors [] = 'Опис новини не може бути порожнім';
        if (empty($row['text']))
            $errors [] = 'Текст новини не може бути порожнім';
        if (count($errors) > 0) {
            return $errors;
        } else
            return true;
    }

    public function addComment($comment)
    {
        $fields = ['text', 'news_id', 'user_id'];
        $commentFiltered = Utils::ArrayFilter($comment, $fields);
        $id = \core\Core::getInstance()->getDB()->insert('comments', $commentFiltered);
        if ($id != null)
            return true;
        return false;
    }

    public function getLastCommentsByNewsId($news_id)
    {
        return \core\Core::getInstance()->getDB()->select('comments', '*', ['news_id' => $news_id], null, 5);
    }
}