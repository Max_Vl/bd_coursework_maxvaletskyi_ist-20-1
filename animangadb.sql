-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Чрв 06 2024 р., 08:35
-- Версія сервера: 10.3.36-MariaDB
-- Версія PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `animangadb`
--

-- --------------------------------------------------------

--
-- Структура таблиці `anime`
--

CREATE TABLE `anime` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `episodes_count` int(11) NOT NULL,
  `genres` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` float NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `studio_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `release_date` date NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `anime`
--

INSERT INTO `anime` (`id`, `title`, `type`, `episodes_count`, `genres`, `status`, `rating`, `description`, `studio_name`, `release_date`, `photo`) VALUES
(1, 'Наруто', 'Багатосерійне', 220, '1, 6, 8', 'Закінчене', 7.9, '<p>За мить до народження Наруто Узумакі величезний демон, відомий як Кюубі, Дев\'ятихвостий Лис, напав на Конохаґакуре, Село Прихованого Листя, і посіяв хаос. Щоб покласти край безчинствам Кюубі, лідер села, Четвертий Хокаге, пожертвував своїм життям і запечатав жахливого звіра всередині новонародженого Наруто. Тепер Наруто - гіперактивний і тупоголовий ніндзя, який досі живе в Конохаґакуре. Остерігаючись Кюубі всередині себе, Наруто намагається знайти своє місце в селі, в той час як його палке бажання стати Хокаге Конохаґакуре приводить його не лише до нових друзів, але й до смертельних ворогів.</p>', 'TV Tokyo', '2002-10-03', '1_66600c38d9e8c'),
(4, 'Ван Піс', 'Багатосерійне', 1107, '1, 6, 8, 9', 'Онгоінг', 8.6, '<p>Золотий Роджер був відомий як «Король піратів», найсильніший і найвідоміший з тих, хто плавав Великою лінією. Захоплення і страта Роджера Світовим Урядом принесла зміни в усьому світі. Його останні слова перед смертю розкрили існування найбільшого скарбу у світі - Ван Пісу. Саме це одкровення призвело до Великої Епохи Піратів, людей, які мріяли знайти One Piece, що обіцяє необмежену кількість багатства і слави, і, цілком можливо, вершину слави і титул Піратського Короля. Зустрічайте Мавпочку Луффі, 17-річного хлопця, який кидає виклик вашому стандартному визначенню пірата. На відміну від популярного образу злого, запеклого, беззубого пірата, що грабує села заради розваги, Луффі став піратом з чистого дива: думка про захопливу пригоду, яка приведе його до інтригуючих людей і, врешті-решт, до обіцяного скарбу. Йдучи слідами свого героя дитинства, Луффі та його команда подорожують Великою Лінією, переживаючи божевільні пригоди, розкриваючи темні таємниці та борючись із сильними ворогами, і все це заради того, щоб дістатися до найбажанішого з усіх багатств - Ван Піс.</p>', 'Toei Animation', '1990-10-20', '4_66600cdb8138f'),
(5, 'Bleach', 'Багатосерійне', 366, '1, 6', 'Закінчене', 7.8, '<p>Ічіґо Куросакі - звичайний старшокласник, аж поки на його родину не нападає Порожній, злий дух, що прагне поглинути людські душі. Саме тоді він зустрічає Шініґамі на ім\'я Рукія Кучікі, яка отримує поранення, захищаючи сім\'ю Ічіґо від нападника. Щоб врятувати свою сім\'ю, Ічіґо приймає пропозицію Рукії перейняти її силу і стає Шініґамі. Однак, оскільки Рукія не може відновити свої сили, Ічіґо отримує складне завдання - полювати на Порожніх, які заполонили їхнє місто. Однак у своїй боротьбі він не самотній, адже згодом до нього приєднуються його друзі - однокласники Оріхіме Іноуе, Ясутора Садо та Урюу Ішіда, - кожен з яких має свої унікальні здібності. Поки Ічіґо та його товариші звикають до своїх нових обов\'язків і підтримують один одного на полі бою та поза ним, молодий Шініґамі незабаром дізнається, що Порожні - не єдина реальна загроза для людського світу</p>', 'Studio Pierrot', '2004-10-06', '5_666016ad70d10'),
(6, 'Fullmetal Alchemist: Brotherhood', 'Багатосерійне', 64, '1, 6, 10', 'Закінчене', 9.2, '<p>«Для того, щоб щось отримати, потрібно втратити щось рівноцінне». Алхімія пов\'язана з цим законом еквівалентного обміну - те, що молоді брати Едвард і Альфонс Елрік усвідомлюють лише після спроби людської трансмутації: єдиного забороненого акту алхімії. Вони платять страшну ціну за свій переступ - Едвард втрачає ліву ногу, Альфонс - фізичне тіло. Лише завдяки відчайдушній жертві правої руки Едварда йому вдається прикріпити душу Альфонса до обладунків. Спустошений і самотній, саме надія на те, що вони обидва врешті-решт повернуться до своїх початкових тіл, дає Едварду натхнення отримати металеві кінцівки під назвою «автопошта» і стати державним алхіміком, Повнометалевим алхіміком. Через три роки пошуків брати шукають філософський камінь - міфічну реліквію, яка дозволяє алхіміку подолати закон еквівалентного обміну. Навіть маючи на своєму боці військових союзників - полковника Роя Мустанга, лейтенанта Різу Яструбине Око та підполковника Мейса Г\'юза, брати виявляються втягнутими в загальнонаціональну змову, яка веде їх не лише до справжньої природи невловимого філософського каменю, а й до темної історії їхньої країни. Поміж пошуком серійного вбивці та перегонами з часом Едвард і Альфонс мусять запитати себе, чи те, що вони роблять, зробить їх знову людьми... чи забере їхню людяність.</p>', 'Bones', '2005-04-05', '6_66601878e17e5'),
(7, 'Your Name', 'Фільм', 1, '7, 9', 'Закінчене', 8.9, '<p>Міцуха Міямізу, старшокласниця, мріє жити життям хлопця в галасливому Токіо - мрія, яка різко контрастує з її теперішнім життям у сільській місцевості. Тим часом у місті Такі Тачібана живе насиченим життям старшокласника, перебиваючись підробітками та сподіваючись на майбутнє в архітектурі.<br>Одного дня Міцуха прокидається в чужій кімнаті і раптом виявляє, що живе життям своєї мрії в Токіо - але в тілі Такі! В іншому місці Такі виявляє, що живе життям Міцухи у скромній сільській місцевості. У пошуках відповіді на цей дивний феномен вони починають шукати одне одного.<br>У центрі сюжету «Кімі но на ва» - вчинки Міцухи і Такі, які починають драматично впливати на життя одне одного, сплітаючи їх у тканину, скріплену долею та обставинами.</p>', 'CoMix Wave Films', '2016-08-16', '7_66601942cc5b2'),
(8, 'Naruto Mission: Protect the Waterfall Village', 'OVA', 1, '1, 6', 'Закінчене', 6.8, '<p>Звичайна місія рангу С перетворюється на повномасштабну битву, коли на село Приховане падіння нападають ворожі ніндзя. Тепер Наруто, Саске і Сакура повинні допомогти лідеру Прихованого падіння Шибукі захистити своє село і показати йому, що таке бути героєм.</p>', 'Studio Pierrot', '2003-12-20', '8_666019ebb5115'),
(9, 'DARLING in the FRANXX', 'Багатосерійне', 24, '5, 7, 9', 'Закінчене', 7.4, '<p>У далекому майбутньому людство опинилося на межі вимирання через гігантських звірів, відомих як клаксозаври, які змусили людей, що вижили, сховатися у величезних містах-фортецях під назвою Плантації. Діти, що виховуються тут, навчаються пілотувати гігантські мехи, відомі як FranXX - єдину зброю, яка ефективно бореться з клаксозаврами - в парах хлопчик-дівчинка. Вирощені з єдиною метою - пілотувати ці машини, ці діти нічого не знають про зовнішній світ і здатні довести своє існування, лише захищаючи свою расу. Хіро, пілот FranXX, втратив мотивацію та впевненість у собі після того, як провалив тест на профпридатність. Прогулюючи випускну церемонію свого класу, Хіро усамітнюється біля лісового озера, де зустрічає загадкову дівчину з двома рогами, що ростуть з голови. Вона представляється своїм кодовим ім\'ям Нуль Два, яке, як відомо, належить сумнозвісному пілоту FranXX, відомому як «Вбивця напарників». Перш ніж Хіро встигає переварити цю зустріч, плантацію здригається від раптового нападу клаксозавра. Нуль Два атакує істоту на своєму Франксі, але той зазнає серйозних пошкоджень у сутичці і падає поруч з Хіро. Знайшовши свого напарника мертвим, Зеро Два запрошує Хіро пілотувати меху разом з нею, і дует з легкістю перемагає клаксозавра в наступній сутичці. З новою напарницею на боці Хіро отримав шанс спокутувати свої минулі невдачі, але якою ціною?</p>', 'A-1 Pictures', '2018-01-13', '9_66601abe8b310'),
(10, 'Jewelpet Magical Change', 'Багатосерійне', 39, '2', 'Закінчене', 6.4, '<p>Історія розповідає про пригоди кроликоподібної Рубі, її давньої найкращої подруги-людини Ейрі Кірари та інших на шляху до відновлення Коштовного замку, який впав з неба посеред міста через невіру людей у магію. Щоб відновити віру людей у магію, тваринки перетворюються на людей, щоб дізнатися про них більше</p>', 'Studio Deen', '2015-04-04', '10_66601b6ce0256');

-- --------------------------------------------------------

--
-- Структура таблиці `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `news_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `comments`
--

INSERT INTO `comments` (`id`, `text`, `news_id`, `user_id`) VALUES
(1, '<p>11515пнекавк</p>', 9, 4),
(2, '<p>Гарна пісня! Вітаємо Yaosobi!</p>', 15, 4);

-- --------------------------------------------------------

--
-- Структура таблиці `genres`
--

CREATE TABLE `genres` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `genres`
--

INSERT INTO `genres` (`id`, `name`) VALUES
(1, 'Шонен'),
(2, 'Шоджо'),
(3, 'Сейнен'),
(4, 'Дзьосей'),
(5, 'Меха'),
(6, 'Магія/Фентезі'),
(7, 'Романтика'),
(8, 'Комедія'),
(9, 'Драма'),
(10, 'Жахи/Трилер');

-- --------------------------------------------------------

--
-- Структура таблиці `manga`
--

CREATE TABLE `manga` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `genres` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chapters_count` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `release_date` date NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `manga`
--

INSERT INTO `manga` (`id`, `title`, `genres`, `chapters_count`, `status`, `author`, `description`, `release_date`, `photo`) VALUES
(1, 'Solo Leveling', '1, 6', 200, 'Закінчене', 'Sung-Lak Jang', '<p>10 років тому, після того, як відкрилися «Ворота», що з\'єднали реальний світ зі світом монстрів, деякі звичайні, повсякденні люди отримали силу полювати на монстрів у межах Воріт. Вони стали відомими як «Мисливці». Проте не всі Мисливці є могутніми. Мене звати Сонг Джин Ву, я Мисливець рангу Е. Я той, кому доводиться ризикувати життям у найнижчому підземеллі, «Найслабшому у світі». Не маючи жодних навичок, я ледве заробляв необхідні гроші, б\'ючись у низькорівневих підземеллях... принаймні, поки не знайшов приховане підземелля з найскладнішим рівнем складності серед підземель D-рангу! Зрештою, коли я приймав смерть, я раптом отримав дивну силу, журнал квестів, який міг бачити тільки я, секрет підвищення рівня, про який знав тільки я! Якщо я тренувався відповідно до своїх квестів і полював на монстрів, мій рівень зростав. Перетворившись із найслабшого мисливця на найсильнішого мисливця S-рангу!</p>', '2016-07-25', '1_66600d60767bd'),
(3, 'The Beginning After The End', '1, 6, 7, 8, 9', 175, 'Онгоінг', 'Turtleme', '<p>Король Грей має неперевершену силу, багатство і престиж у світі, де панує військова сила. Однак самотність тісно переслідує тих, хто має велику владу. Під гламурною зовнішністю могутнього короля ховається оболонка людини, позбавленої мети і волі.</p><p>Перевтілившись у новий світ, сповнений магії та чудовиськ, король отримує другий шанс пережити своє життя. Однак виправлення помилок минулого буде не єдиним його викликом. Під миром і процвітанням нового світу ховається підводна течія, що загрожує знищити все, заради чого він працював, ставлячи під сумнів його роль і причину повторного народження.</p>', '2018-07-16', '3_66600da125656'),
(4, 'Kimetsu No Yaiba', '1, 6', 206, 'Закінчене', 'Gotouge Koyoharu', '<p>Тандзіро - найстарший син у сім\'ї, який втратив батька. Одного дня Тандзіро вирушає до іншого міста, щоб продати деревне вугілля. Замість того, щоб повернутися додому, він залишається на ніч у чужому домі через чутки про демона, що мешкає неподалік у горах. Коли він повертається додому наступного дня, на нього чекає жахлива трагедія.</p>', '2016-02-15', '4_6660254d7bdf7'),
(5, 'Onepunch-Man', '1, 6, 8', 202, 'Онгоінг', 'Murata Yuusuke', '<p>One punch-Man імітує життя середньостатистичного героя, який виграє всі свої бої лише одним ударом! Саме тому його називають Одноударником. Ця історія відбувається у вигаданому місті Z. Світ сповнений таємничих істот, лиходіїв та монстрів, які спричиняють руйнування та хаос. Щоб захистити городян від усього лихого та ворогів, було створено асоціацію героїв. Люди з надлюдськими здібностями можуть зареєструватися в асоціації, яка захищає громадян. Там вони повинні будуть пройти серію тестів, щоб визначити свої здібності та клас, до якого вони належать. Клас S - найвищий, а клас С - найнижчий.</p>', '2012-12-04', '5_666025e10f91d');

-- --------------------------------------------------------

--
-- Структура таблиці `marks`
--

CREATE TABLE `marks` (
  `id` int(11) NOT NULL,
  `story` int(11) NOT NULL DEFAULT 0,
  `drama` int(11) NOT NULL DEFAULT 0,
  `characters` int(11) NOT NULL DEFAULT 0,
  `atmosphere` int(11) NOT NULL DEFAULT 0,
  `id_review` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `marks`
--

INSERT INTO `marks` (`id`, `story`, `drama`, `characters`, `atmosphere`, `id_review`) VALUES
(3, 9, 9, 8, 10, 1),
(5, 8, 7, 10, 6, 3),
(6, 10, 8, 9, 10, 4);

-- --------------------------------------------------------

--
-- Структура таблиці `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `datetime` datetime NOT NULL,
  `datetime_lastEdit` datetime DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `news`
--

INSERT INTO `news` (`id`, `title`, `short_description`, `text`, `datetime`, `datetime_lastEdit`, `photo`, `user_id`) VALUES
(1, 'test', '        test', '        test', '2024-01-24 17:25:25', NULL, '...photo...', 1),
(2, 'test2', '        test2', '        test2', '2024-01-24 18:00:59', NULL, '...photo...', 1),
(5, 'Test News With Photo', '<p>Test News With Photo</p>', '<p>Test News With Photo</p>', '2024-01-26 11:57:07', NULL, '...photo...', 1),
(6, 'Test photo 2', '<p>Test photo 2</p>', '<p>Test photo 2</p>', '2024-01-26 12:07:41', '2024-01-26 13:32:32', '6_65b38a40980bb', 1),
(7, 'Test editor news', '<p>Test editor news</p>', '<p>Test editor news</p>', '2024-01-26 13:52:49', '2024-01-26 13:52:49', '7_65b38f011f224', 2),
(8, 'Test for pagination', '<p>Test for pagination</p>', '<p>Test for pagination</p>', '2024-01-29 17:25:37', NULL, '...photo...', 1),
(9, 'Test for pagination 2', '<p>Test for pagination 2</p>', '<p>Test for pagination 2</p>', '2024-01-29 17:47:05', NULL, '...photo...', 1),
(14, 'My Wife Has No Emotion Anime Brings the Love in Wedding Illustration', '<h2>Two more cast members were also revealed</h2>', '<p>Love is in the air between man and machine in next season’s<i><strong> My Wife Has No Emotion</strong></i> TV anime adaptation of the manga of the same name by Jiro Sugiura. To celebrate the romance between the unlikely couple, a brand new illustration of the couple was released that is featured on the cover on <i>Monthly Comic Flapper </i>today.</p><figure class=\"image\"><img style=\"aspect-ratio:1280/1810;\" src=\"https://a.storyblok.com/f/178900/1280x1810/98c1ccb1f7/my-wife-has-no-emotion-wedding-illustration.jpg/m/filters:quality(95)format(webp)\" alt=\"My Wife Has No Emotion Wedding Illustration\" width=\"1280\" height=\"1810\"></figure><p>Alongside the illustration, two new cast members were announced for the series:</p><figure class=\"image\"><img style=\"aspect-ratio:1280/2385;\" src=\"https://a.storyblok.com/f/178900/1280x2385/fdcc3a97a7/my-wife-has-no-emotion-rihito-visual.jpg/m/filters:quality(95)format(webp)\" alt=\"My Wife Has No Emotion rihito visual\" width=\"1280\" height=\"2385\"></figure><p><strong>Rihito Saionji</strong>&nbsp;voiced by Risae Matsuda (Chika Sawada in <a href=\"https://www.crunchyroll.com/series/GNVHKNG05/kageki-shojo?utm_source=news_cr&amp;utm_medium=editorial_cr&amp;utm_campaign=news_en&amp;referrer=news_cr_editorial_cr_news\"><i><strong>Kageki Shojo!!</strong></i></a>)</p><figure class=\"image\"><img style=\"aspect-ratio:1280/1578;\" src=\"https://a.storyblok.com/f/178900/1280x1578/a93c003683/my-wife-has-no-emotion-mamoru-visual.jpg/m/filters:quality(95)format(webp)\" alt=\"My Wife Has No Emotion mamoru visual\" width=\"1280\" height=\"1578\"></figure><p><strong>Mamoru</strong>&nbsp;voiced by Yuki Wakai (Itsuki Sumeragi in <i><strong>Kakegurui</strong></i>)</p><blockquote><h4><strong>RELATED: </strong><a href=\"https://www.crunchyroll.com/news/latest/2024/5/2/my-wife-has-no-emotion-anime-key-visual-new-cast\"><strong>My Wife Has No Emotion Anime Reveals New Key Visual, More Cast</strong></a></h4></blockquote><p><i><strong>My Wife Has No Emotion</strong></i>&nbsp;is being directed by Fumihiro Yoshimura (<i><strong>Endo and Kobayashi Live! The Latest on Tsundere Villainess Lieselotte</strong></i>) at Tezuka Productions, with series composition by Mitsutaka Hirota (<a href=\"https://www.crunchyroll.com/series/G6QWV3976/rent-a-girlfriend?utm_source=news_cr&amp;utm_medium=editorial_cr&amp;utm_campaign=news_en&amp;referrer=news_cr_editorial_cr_news\"><i><strong>Rent-a-Girlfriend</strong></i></a>) and character designs by Tsuyoshi Sasaki (<a href=\"https://www.crunchyroll.com/series/GEXH3WP97/girlfriend-girlfriend?utm_source=news_cr&amp;utm_medium=editorial_cr&amp;utm_campaign=news_en&amp;referrer=news_cr_editorial_cr_news\"><i><strong>Girlfriend, Girlfriend</strong></i></a>&nbsp;chief animation director). The series is set to premiere on July 2.</p><p>Seven Seas Entertainment, which&nbsp;<a href=\"https://sevenseasentertainment.com/series/my-wife-has-no-emotion/\">publishes</a>&nbsp;the manga\'s official English version, describes the series as such:</p><p><i>Takuma is a single guy who does nothing but go to work and come home. Too tired to do chores, he decides to get a robot to cook and keep house. “Mina-chan” is such a good housekeeper, Takuma jokes that she should become his wife. Mina takes Takuma’s joke seriously, and slowly the two start doing more things together, like having a picnic outside. As time goes by, Takuma starts to fall for Mina, but can a human and a robot ever have an equal, loving relationship?</i></p>', '2024-06-05 11:49:29', '2024-06-05 11:49:29', '14_66602699b77ef', 4),
(15, 'YOASOBI\'s Frieren: Beyond Journey\'s End опенінг перевищив 100 мільйонів переглядів', '<h2>Сама пісня досягла 100 мільйонів прослуховувань у січні</h2>', '<p>Офіційний твіттер-акаунт аніме-франшизи Frieren: За межами кінця подорожі« підтвердив 4 червня, що музичний кліп на першу вступну пісню аніме “Yusha” (»Хоробрі\") у виконанні японського музичного гурту YOASOBI, що складається з двох учасників, нарешті перевищив позначку в 100 мільйонів переглядів на YouTube.</p><p>Відео було розміщене на їхньому офіційному каналі YouTube 29 вересня 2023 року, а потім досягло 10 мільйонів переглядів всього за 12 днів. На момент написання цієї статті відео переглянули 103 960 800 разів.</p><p>«Юша» вперше вийшла у цифровому форматі на лейблі Sony Music Entertainment 29 вересня 2023 року. За даними Billboard Japan, у січні 2024 року пісня досягла 100 мільйонів прослуховувань. Англомовна версія пісні «The Brave» вийшла 22 листопада 2023 року. Як четвертий сингл гурту, CD-сингл «Yusha» вийшов у Японії 23 грудня 2023 року, посівши 3 місце у щотижневому чарті синглів Oricon.</p><p>Разом з «Kaibutsu» (початкова тема BEASTERS), «Shukufuku» (початкова тема Mobile Suit Gundam the Witch from Mercury) та останньою «Idol» (початкова тема Oshi no Ko), «Yusha» стала однією з найуспішніших тематичних пісень для аніме в кар\'єрі YOASOBI.</p><p>Телевізійна аніме-адаптація фентезійної манги Канехіто Ямади «Frieren: Beyond Journey\'s End фентезійної манги Канехіто Ямади вийшла в Японії у 28 епізодах з 29 вересня 2023 року по 22 березня 2024 року. Crunchyroll транслює всі серії для користувачів у Північній Америці, Центральній Америці, Південній Америці, Європі, Африці, Океанії, Близькому Сході, СНД та Індії.</p>', '2024-06-05 11:52:07', '2024-06-05 11:52:07', '15_66602737309d9', 4),
(16, 'Телевізійне аніме ATRI - «Мої дорогі моменти» дебютує 13 липня', '<p>Новий трейлер, ключові візуальні ефекти та багато іншого про майбутню екранізацію візуального роману</p>', '<p>ATRI -My Dear Moments-, майбутнє телевізійне аніме, засноване на візуальному романі, розробленому Frontwing та Makura і опублікованому ANIPLEX.EXE, представило новий ключовий візуальний ряд (нижче), новий трейлер та нову інформацію про офіційні початкову та кінцеву пісні серіалу.</p><p>ATRI -My Dear Moments- транслюватиметься в Японії на TOKYO MX, BS11, Gunma TV та Tochigi TV, починаючи з 13 липня 2025 року, з додатковою трансляцією на AT-X, починаючи з 15 липня 2024 року.</p><figure class=\"media\"><oembed url=\"https://youtu.be/l5onThIXL9M\"></oembed></figure><p>Nogizaka46 виконує початкову тему серіалу, яка називається «Ano Hikari» («Це світло»). 22/7 виконує фінальну тему серіалу, яка називається «YES to NO no Ma ni» («Простір між ТАК і НІ»).</p><p>Макото Като є режисером телевізійного аніме ATRI - My Dear Moments - на анімаційній студії TROYCA. Юккі Ханада відповідає за композицію та сценарії серіалу, Мічіо Сато - за дизайн персонажів і є головним режисером анімації, а Фуміморі Мацумото - за музику.</p><p>Історія візуального роману ATRI -My Dear Moments- описується наступним чином:</p><p><i>У недалекому майбутньому раптовий і незрозумілий підйом рівня моря залишив більшу частину людської цивілізації під водою.</i></p><p>&nbsp;</p><p><i>Ікаруга Нацукі, хлопець, який кілька років тому втратив матір і ногу внаслідок нещасного випадку, повертається розчарованим із суворого життя у великому місті, щоб знайти свій старий сільський будинок, наполовину поглинутий морем.</i></p><p>&nbsp;</p><p><i>Залишившись без сім\'ї, він має лише корабель і підводний човен, які залишила йому у спадок бабуся-океанолог, та її борги.</i></p><p>&nbsp;</p><p><i>Його єдина надія відновити втрачені мрії про майбутнє - скористатися можливістю, яку йому пропонує підозріла колекторка Катрін. Вони вирушають на пошуки затонулих руїн лабораторії його бабусі, щоб знайти скарб, який, за чутками, вона там залишила.</i></p><p>&nbsp;</p><p><i>Але те, що вони знаходять - не багатство чи коштовності, а дивну дівчину, яка спить у труні на дні моря.</i></p><p><i>Атрі.</i></p><p>&nbsp;</p><p><i>Атрі - робот, але її зовнішній вигляд і багатство емоцій змусять будь-кого подумати, що вона жива, дихаюча людина. На знак подяки за порятунок вона робить заяву Нацукі.</i></p><p>&nbsp;</p><p><i>«Я хочу виконати останній наказ мого господаря. А поки я цього не зроблю, я буду твоєю ногою!»</i></p><p>&nbsp;</p><p><i>У маленькому містечку, яке повільно огортає океан, починається незабутнє літо для цього хлопчика і цієї загадкової дівчини-робота...</i></p>', '2024-06-05 11:55:02', '2024-06-05 11:55:02', '16_666027e654315', 4);

-- --------------------------------------------------------

--
-- Структура таблиці `reviews`
--

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `review_type` varchar(255) NOT NULL,
  `short_text` text NOT NULL,
  `text` text NOT NULL,
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `reviews`
--

INSERT INTO `reviews` (`id`, `user_id`, `target_id`, `review_type`, `short_text`, `text`, `datetime`) VALUES
(1, 4, 1, 'anime', 'Наруто найкращий Шонен сьогодення?????', '<p>Я бачив багато аніме протягом багатьох років. Я бачив те, що люди називають «хорошим», я бачив «погане». З часом я зрозумів і прийняв, що не всім щось може подобатися чи не подобатися. Незалежно від того, що це за аніме (або фільм, або книга, або гра тощо), все зводиться до особистих уподобань. Тож, як дізнатися, чи сподобається вам щось, чого ви ніколи не бачили, але так багато про нього чули? Коли я вперше почав дивитися «Наруто», я побачив у ньому потенціал. З першого епізоду вони представили окремий світ з окремими персонажами. Звичайно, були використані архетипи (хлопчик, який мріє стати великим, старий дідусь, вчитель, суперник). Перший епізод показав емоційність і глибину головного героя і те, з чим йому доведеться боротися, а також динаміку його взаємодії з іншими персонажами та їхні дивацтва.</p><p>Приблизно через десять епізодів сюжетна дуга, за якою ми зараз спостерігаємо, здавалася мені не надто важливою для основної історії. Деякі епізоди затягували битви, і в мене виникло відчуття, що аніме буде з тих, де герої переходять з однієї місцевості в іншу, розвиваючи свої навички, засвоюючи моральні уроки тощо, але не буде великого сюжету, історії «Б», яка б пов\'язувала серіал воєдино. Я також хвилювався, що він впаде в посередність. Не хочу нічого видавати, але до кінця цієї арки (приблизно на 19 серії) я змінив свою думку.</p>', '2024-06-04 13:40:45'),
(3, 4, 1, 'anime', 'Чудові моменти, але забагато філлерів', '<p>Ви, напевно, думаєте: «Ну, я можу просто пропустити філери, шукаючи філерні епізоди вище». Це чудово, але це не єдина проблема серіалу. Справа в тому, що навіть в епізодах, які не є філерами, відчувається, що вони затягують час. Якби шоу було стиснене до 90/100 серій, це було б чудово. Хайпові моменти справді хайпові, а ніндзя - це круто.</p>', '2024-06-05 12:06:11'),
(4, 4, 1, 'manga', 'Це щось із чимось!', '<p>І ви не пошкодуєте!</p><p>Я вже більше року дивлюся цей серіал, перша серія викликала у мене певний захват, але друга серія - це те, з чого варто почати!</p><p>Я рекомендую переглянути ці перші три серії разом, щоб відчути атмосферу фільму, відчуваєте мене? Перший епізод не зовсім сподобався деяким, включаючи мене, але повірте мені, якщо ви не продовжите, ви багато чого втратите!</p><p>Арт-гра на іншому рівні - малюнок, рухи персонажів і кольори, що використовуються - просто вогонь. Це цілий настрій, щось, з чим ви не стикаєтесь щодня.</p><p>Ця дурнувата, але трохи моторошна посмішка? Це те, що зачепило мене рік тому, а тепер ми отримуємо її передісторію, чистий вогонь.</p><p>Вступ наче в кінотеатрі, відчувається, що це справжнє кіно. Вступ, щоправда, потребує доопрацювання, він трохи не дуже, сподіваюся, вони над ним попрацюють!</p><p>У цьому візуальному світі оповіді кожен кадр розповідає історію, і мова йде не лише про сюжет. Стиль анімації говорить багато про що, показуючи шалену відданість творців, і я вже не можу дочекатися наступної суботи!</p>', '2024-06-05 12:09:47');

-- --------------------------------------------------------

--
-- Структура таблиці `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `role`, `nickname`) VALUES
(1, 'ist201_vms@student.ztu.edu.ua', '81dc9bdb52d04dc20036dbd8313ed055', 'admin', 'Mutambikos'),
(2, 'ist201_vms2@student.ztu.edu.ua', '202cb962ac59075b964b07152d234b70', 'editor', 'Mutambi1'),
(3, 'ist201_vms3@student.ztu.edu.ua', '202cb962ac59075b964b07152d234b70', 'user', 'Mutambi2'),
(4, 'max.valetskyi.off@gmail.com', '736e7ae45b1789f7eb60aeb2427d42c7', 'admin', 'Mubi0');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `anime`
--
ALTER TABLE `anime`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `news_id` (`news_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Індекси таблиці `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `manga`
--
ALTER TABLE `manga`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `marks`
--
ALTER TABLE `marks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_review` (`id_review`);

--
-- Індекси таблиці `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Індекси таблиці `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Індекси таблиці `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `anime`
--
ALTER TABLE `anime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблиці `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблиці `genres`
--
ALTER TABLE `genres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблиці `manga`
--
ALTER TABLE `manga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблиці `marks`
--
ALTER TABLE `marks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблиці `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблиці `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблиці `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`news_id`) REFERENCES `news` (`id`),
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Обмеження зовнішнього ключа таблиці `marks`
--
ALTER TABLE `marks`
  ADD CONSTRAINT `marks_ibfk_1` FOREIGN KEY (`id_review`) REFERENCES `reviews` (`id`);

--
-- Обмеження зовнішнього ключа таблиці `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `news_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Обмеження зовнішнього ключа таблиці `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
