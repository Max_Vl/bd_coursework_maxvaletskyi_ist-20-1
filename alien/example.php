<?php

interface WorkProgramBuilderInterface
{
    public function setStudyMaterial(StudyMaterial $studyMaterial): void;

    public function setObjectives(array $objectives): void;

    public function setSchedule(array $schedule): void;

    public function setAssessment(array $assessment): void;

    public function build(): WorkProgram;
}

class WorkProgramBuilder implements WorkProgramBuilderInterface
{
    private $studyMaterial;
    private $objectives = [];
    private $schedule = [];
    private $assessment = [];

    public function setStudyMaterial(StudyMaterial $studyMaterial): void
    {
        $this->studyMaterial = $studyMaterial;
    }

    public function setObjectives(array $objectives): void
    {
        $this->objectives = $objectives;
    }

    public function setSchedule(array $schedule): void
    {
        $this->schedule = $schedule;
    }

    public function setAssessment(array $assessment): void
    {
        $this->assessment = $assessment;
    }

    public function build(): WorkProgram
    {
        return new WorkProgram(
            $this->studyMaterial,
            $this->objectives,
            $this->schedule,
            $this->assessment
        );
    }
}

class WorkProgram
{
    private $studyMaterial;
    private $objectives;
    private $schedule;
    private $assessment;

    public function __construct(
        StudyMaterial $studyMaterial,
        array $objectives,
        array $schedule,
        array $assessment
    ) {
        $this->studyMaterial = $studyMaterial;
        $this->objectives = $objectives;
        $this->schedule = $schedule;
        $this->assessment = $assessment;
    }

    // ... методи класу ...
}

// Приклад використання

$builder = new WorkProgramBuilder();
$builder->setStudyMaterial(new StudyMaterial('Навчальний посібник з PHP'));
$builder->setObjectives([
    'Ознайомитися з основами PHP',
    'Навчитися писати прості PHP-скрипти',
    'Опанувати основні концепції ООП в PHP'
]);
$builder->setSchedule([
    '1 тиждень: Вступ до PHP',
    '2 тиждень: Оператори та змінні',
    '3 тиждень: Умовні оператори',
    '4 тиждень: Цикли'
]);
$builder->setAssessment([
    'Тестування',
    'Практичні завдання',
    'Підсумковий проект'
]);

$workProgram = $builder->build();
