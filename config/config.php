<?php
$Config =
    [
        'Database' =>
        [
            'Server'   => 'localhost',
            'Username' => 'root',
            'Password' => '',
            'Database' => 'animangadb'
        ],
        'NewsCount' => 5,
        'ReviewsCount' => 5,
        'AnimaAndMangaCount' => 7
    ];